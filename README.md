# BendyStraw

An app for managing **NewPipe** databases.

If you don’t know it, **NewPipe** is a privacy-friendly, ad-free Android app for accessing YouTube. It’s a great piece of software and I use it all the time.

**NewPipe** works without an account. You can still subscribe to channels, save playlists and do other useful things, but everything is saved locally on your device (which I love).

I have several Android devices, and over time I ended up with different playlists and subscriptions on each device, and sometimes wasn’t sure which device I needed to use to find a certain song or video. To help with this I made **BendyStraw**.

**BendyStraw** imports `NewPipeData-*.zip` files and lets you:

- Open multiple `zip`s at the same time, so you can combine data from several devices
- `Delete` `Copy` `Move` `Rename` your custom playlists
- `Delete` `Copy` `Move` streams from one playlist to another
- `Delete` `Copy` `Move` channel subscriptions between databases
- `Delete` `Copy` `Move` remote (bookmarked) playlists
- Re-order playlists, sorting the streams by `Title`, `Channel` or `Length` (just tap the column headings in the tables)
- Streams (audio/video) can be opened directly from **BendyStraw**, as the URLs are clickable
    - If you set video links (in your Android settings) to open in **NewPipe** you can make a split-screen view and jump around your playlists
- Export playlist as raw text, for example to be used with [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- Dark/light themes

After editing simply export a new `zip` file, then import it back into **NewPipe**.


## Installation

### F-droid

This app is available on F-Droid, just search for 'bendystraw' (all one word).





## Build Instructions

This app is built with **Flutter**, you'll need a recent version installed ([install instructions](https://docs.flutter.dev/get-started/install)).


### Dependencies

Dependencies:

- Flutter 
- sqlite3 and libraries, eg in Ubuntu:
```
sudo apt install sqlite3 libsqlite3-dev
```


### Flutter Analytics

_This only applies to the **Flutter** tools used while building the app. **BendyStraw** itself does not use or send any telemetry or analytics anywhere._

By default the **Flutter** tools send telemetry/analytics. To disable this run:
```
flutter --disable-telemetry
```


### Build APKs (each architecture in a separate file)

To build the APKs for Android run the following from the application root directory:

```
flutter build apk --split-per-abi
```

This will build APKs for 3 architectures at:

```
build/app/outputs/apk/release/app-arm64-v8a-release.apk
build/app/outputs/apk/release/app-armeabi-v7a-release.apk
build/app/outputs/apk/release/app-x86_64-release.apk

```


### Fat APK  (one file containing all architectures)

To build the fat Android APK (single installer for all architectures) run the following from the application root directory:

```
flutter build apk
```


This will create an apk at:

```
build/app/outputs/apk/release/app-release.apk
```




## FAQs


### What's in the `zip` files?

**NewPipe** exports a `zip` file, containing 2 files:

- `newpipe.db`: A database which has all the info **BendyStraw** uses, such as your playlists and channel subscriptions, plus other things like your watch history
- `newpipe.settings`: Your **NewPipe** settings and preferences

For simplicity **BendyStraw** sometimes refers to the `zip` file as the database, because that's the file **NewPipe** works with for imports/exports.


### Is my data private?

Yes! This is important to me as a developer. **BendyStraw** only works with the tables in the database which it requires to let you do your editing. It doesn't look into other things like your settings or watch history and it doesn't send your data to any servers or share it with anybody. There's no tracking and no adverts or anything like that.


### What happens to my **NewPipe** settings?

**BendyStraw** doesn't look inside them or change them. 

**If you edit `zip`s from multiple devices, or old/archived `zip`s, be aware that your new `zip` will have the settings from whichever original file you opened. So be careful not to overwrite settings with an old version.**

When you import your new `zip` into **NewPipe** the simplest thing to do is just tap 'no' when it asks if you want to also import settings.


### How do I export a database from **NewPipe**?

- In **NewPipe** tap `Settings` `Content` `Export database`
- This will create a file on your device with a name like `NewPipeData-2023-09-01.zip`
- In **BendyStraw**, tap the button with the add/plus icon, and select the file you just exported


### How do I import an edited database back into **NewPipe**?

- In **BendyStraw** tap the export button (the arrow icon on the bottom-right of the screen)
- This will create a new file named like the original but with `-bendy-straw` at the end, so `NewPipeData.zip` is saved as `NewPipeData-bendy-straw.zip`
- In **NewPipe** tap `Settings` `Content` `Import database` and select the file you just saved
- **NewPipe** will ask if you also want to import settings, if you're not sure what this means just tap 'no', your current **NewPipe** settings will remain unchanged


### How do I edit playlist names?

Just tap on a playlist name to start editing it, and tap outside (or press return on the keyboard) to finish.


### I've opened multiple databases but don't see enough tabs

The tabs can scroll off the edge of the screen, swipe left/right on the tab bar to see any tabs which have overflowed.

You can also swipe left or right on any blank area to switch tabs.


### How do I use an exported text playlist?

If you have [yt-dlp](https://github.com/yt-dlp/yt-dlp) installed, you can batch download all of the files from the playlist like so:

```
yt-dlp --batch-file my-music-playlist.txt
```

For more info check out [the yt-dlp github](https://github.com/yt-dlp/yt-dlp).


### Can I export an M3U playlist?

I originally did build this functionality, only to find that links to eg YouTube streams no longer work in `m3u` playlists. This is true for the players I tested and to the best of my knowledge. If I'm wrong about this or you have useful info, or would like to see some other export format, please let me know. I'd be happy to revisit this if it's useful.


### Why do I need to grant 'all files access' permission in Android 13?

**BendyStraw** needs to work with files from a different app (**NewPipe**). The way permissions have changed in Android 13 makes this complicated, as document files (ie non-media files such as `.zip`) can only be read from and written to the directory belonging to the app that created them. The only simple way to keep the app easy to use is to ask for these permissions.

**BendyStraw** has no interest in doing anything unexpected to your filesystem. The code is open source, so any developer can check to make sure that it only does what it says it does.




## JSON Playlist Import

**BendyStraw** can import playlists from a JSON file. This is a new and basic feature which has some requirements and limitations.

### Show the JSON Import Button
**The button is hidden by default**
- Go to the preferences screen (hamburger menu in top-right corner)
- Tick the 'Show JSON Import Button' box
- Open at least 1 database  
  _The button only shows when there is a database open, as it imports the JSON into the currently-opened database_




### Limitations

We assume that all playlist entries are **YouTube** streams.  
_Although **NewPipe** is able to handle streams from other services too, for now this import feature is just for **YouTube**_

The JSON import format has been designed to prefer simplicity over richness of data. This comes with some pros and cons:
- Several stream attributes (eg 'uploader URL', 'duration', 'upload date', 'view count', 'thumbnail') are omitted
- After import into **NewPipe**, before the playlist has been played, the above details will be missing (so eg the thumbnail will just be a grey blank)
- The first time each stream is started, **NewPipe** will grab the missing details for the stream and fill in the database




### Requirements

JSON playlists will be imported into whichever database is currently open in **BendyStraw**.

The general (pseudocode) schema is:

```json
{
  "PLAYLIST_NAME": [ARRAY_OF_STREAMS],
  "PLAYLIST_NAME": [ARRAY_OF_STREAMS],
  etc...
}
```


#### `PLAYLIST_NAME`
- Is expected to be unique (if it isn't, identically-named lists will be combined into 1 playlist)
- If a playlist with exactly the same name already exists in the target database, entries from the JSON will be appended to that existing database


#### `ARRAY_OF_STREAMS`

**I use comments in this explanation but remember comments are not valid JSON --- do not include comments in your JSON**
Each stream must be an object with named properties as follows:

```json
{
  // These are required
  "stream_type": "VIDEO_STREAM", // or 'AUDIO_STREAM'
  "title": "Seasonal Affective Disorder",
  "url": "https://www.youtube.com/watch?v=3RJ5GftYT2c",
  "uploader": "Hidden Valley Bushcraft", // Uploader/channel name,

  // These below are optional. NewPipe will update them when the stream is loaded, so they only have meaning on first import before the playlist has been played.
  // It's probably better to omit them but they are allowed if you want to use them.
  "uploader_url": "https://www.youtube.com/channel/UCB7BPYlL4f5j5R1Y7HUgeVg", // Uploader/channel URL 
  "duration": "120", // Length of stream in seconds 
  "view_count": "1273621", // Number of times the stream has been viewed on YouTube --- simple number ie no commas, abbreviations etc 
}
```






#### Complete Example JSON

Below is some complete sample JSON. If this was imported into a database in **BendyStraw**:
- It would either create 2 new playlists ("Tech Things" and "Good Songs")
- Or if playlists with those names already existed then the streams would be appended to them

```JSON
{
  "Tech Things": [
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Free software, free society: Richard Stallman at TEDxGeneva 2014",
        "url": "https://www.youtube.com/watch?v=Ag1AKIl_2GM",
        "uploader": "TEDx Talks",
        "uploader_url": "https://www.youtube.com/channel/UCsT0YIqwnpJCM-mx7-gSA4Q"
    },
    {
      "stream_type": "VIDEO_STREAM",
      "title": "Shenzhen: The Silicon Valley of Hardware (Full Documentary) | Future Cities",
      "url": "https://www.youtube.com/watch?v=SGJ5cZnoodY",
      "uploader": "Wired UK",
      "uploader_url": "https://www.youtube.com/@wireduk"
    }
  ],
  "Good Songs": [
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Aztec Camera - Somewhere In My Heart (Official Music Video)",
        "url": "https://www.youtube.com/watch?v=2w1Q8ZkXZ1Q",
        "uploader": "RHINO",
        "uploader_url": "https://www.youtube.com/channel/UCWEtnEiVwUy7mwFeshyAWLA"
    },
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Imagine Dragons - Thunder",
        "url": "https://www.youtube.com/watch?v=fKopy74weus",
        "uploader": "ImagineDragonsVEVO",
        "uploader_url": "https://www.youtube.com/channel/UCpx_k19S2vUutWUUM9qmXEg"
    },
    {
        "stream_type": "VIDEO_STREAM",
        "title": "𝑰𝒕'𝒔 𝑨 𝑯𝒆𝒂𝒓𝒕𝒂𝒄𝒉𝒆 -Bonnie Tyler (Cover by Yhuan) #gutomversion #hitback #goodvibestambayan #yhuan",
        "url": "https://www.youtube.com/watch?v=AQCm7VWyg-U",
        "uploader": "Yhuan Official",
        "uploader_url": "https://www.youtube.com/channel/UCDxVjSFhfiPGispcK9o0qzw"
    }
  ]
}
```



## Support Development by Donating


Donations are greatly appreciated and help to support development of this and future apps.  

If you like the app and want to say thanks:  

Bitcoin: bc1qtd03k9fnulmxrgwsrruvp053rgal905em2c0cq  
PayPal: donate@mm-dev.rocks  


