## JSON Playlist Import

**BendyStraw** can import playlists from a JSON file. This is a new and basic feature which has some requirements and limitations.

### Show the JSON Import Button
**The button is hidden by default**
- Go to the preferences screen (hamburger menu in top-right corner)
- Tick the 'Show JSON Import Button' box
- Open at least 1 database  
  _The button only shows when there is a database open, as it imports the JSON into the currently-opened database_




### Limitations

We assume that all playlist entries are **YouTube** streams.  
_Although **NewPipe** is able to handle streams from other services too, for now this import feature is just for **YouTube**_

The JSON import format has been designed to prefer simplicity over richness of data. This comes with some pros and cons:
- Several stream attributes (eg 'uploader URL', 'duration', 'upload date', 'view count', 'thumbnail') are omitted
- After import into **NewPipe**, before the playlist has been played, the above details will be missing (so eg the thumbnail will just be a blank grey)
- The first time each stream is started, **NewPipe** will grab the missing details for the stream and fill in the database




### Requirements

JSON playlists will be imported into whichever database is currently open in **BendyStraw**.

The general (pseudocode) schema is:

```json
{
  "PLAYLIST_NAME": [ARRAY_OF_STREAMS],
  "PLAYLIST_NAME": [ARRAY_OF_STREAMS],
  etc...
}
```


#### `PLAYLIST_NAME`
- Is expected to be unique (if it isn't, identically-named lists will be combined into 1 playlist)
- If a playlist with exactly the same name already exists in the target database, entries from the JSON will be appended to that existing database


#### `ARRAY_OF_STREAMS`

Each stream must be an object with named properties as follows:
**I use comments in this explanation but remember comments are not valid JSON --- do not include comments in your JSON**

```json
{
  // These are required
  "stream_type": "VIDEO_STREAM", // or 'AUDIO_STREAM'
  "title": "Seasonal Affective Disorder",
  "url": "https://www.youtube.com/watch?v=3RJ5GftYT2c",
  "uploader": "Hidden Valley Bushcraft", // Uploader/channel name,

  // These below are optional. NewPipe will update them when the stream is loaded, so they only have meaning on first import before the playlist has been played.
  // It's probably better to omit them but they are allowed if you want to use them.
  "uploader_url": "https://www.youtube.com/channel/UCB7BPYlL4f5j5R1Y7HUgeVg", // Uploader/channel URL 
  "duration": "120", // Length of stream in seconds 
  "view_count": "1273621", // Simple number ie no commas, abbreviations etc 
}
```






#### Example JSON

Below is some complete sample JSON. If this was imported into a database in **BendyStraw**:
- It would either create 2 new playlists ("Tech Things" and "Good Songs")
- Or if playlists with those names already existed then the streams would be appended to them

```JSON
{
  "Tech Things": [
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Free software, free society: Richard Stallman at TEDxGeneva 2014",
        "url": "https://www.youtube.com/watch?v=Ag1AKIl_2GM",
        "uploader": "TEDx Talks",
        "uploader_url": "https://www.youtube.com/channel/UCsT0YIqwnpJCM-mx7-gSA4Q"
    },
    {
      "stream_type": "VIDEO_STREAM",
      "title": "Shenzhen: The Silicon Valley of Hardware (Full Documentary) | Future Cities",
      "url": "https://www.youtube.com/watch?v=SGJ5cZnoodY",
      "uploader": "Wired UK",
      "uploader_url": "https://www.youtube.com/@wireduk"
    }
  ],
  "Good Songs": [
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Aztec Camera - Somewhere In My Heart (Official Music Video)",
        "url": "https://www.youtube.com/watch?v=2w1Q8ZkXZ1Q",
        "uploader": "RHINO",
        "uploader_url": "https://www.youtube.com/channel/UCWEtnEiVwUy7mwFeshyAWLA"
    },
    {
        "stream_type": "VIDEO_STREAM",
        "title": "Imagine Dragons - Thunder",
        "url": "https://www.youtube.com/watch?v=fKopy74weus",
        "uploader": "ImagineDragonsVEVO",
        "uploader_url": "https://www.youtube.com/channel/UCpx_k19S2vUutWUUM9qmXEg"
    },
    {
        "stream_type": "VIDEO_STREAM",
        "title": "𝑰𝒕'𝒔 𝑨 𝑯𝒆𝒂𝒓𝒕𝒂𝒄𝒉𝒆 -Bonnie Tyler (Cover by Yhuan) #gutomversion #hitback #goodvibestambayan #yhuan",
        "url": "https://www.youtube.com/watch?v=AQCm7VWyg-U",
        "uploader": "Yhuan Official",
        "uploader_url": "https://www.youtube.com/channel/UCDxVjSFhfiPGispcK9o0qzw"
    }
  ]
}
```
