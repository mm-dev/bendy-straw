# Update version

In `pubspec.yaml`;

- Update `version: 1.1.0+2`


The 'version name' `1.1.0` is what's shown to users.


After the `+` is the 'version code', increment this for F-Droid to recognise that this is a new build.

F-Droid will look at any new (since previous build) tags in the repo to see if they have a higher version code than the previous build. If it finds a match of these 2 conditions new tag with higher version code), this will trigger a new F-Droid build/release.



# Automatically update `local.properties`

According to multiple online sources, after updating the above, the following should be run from the project root in order to update the `android/local.properties` file.

```
./flutterw run
# or
./flutterw pub get
# or
./flutterw build apk
```

For me, only the 3rd option updated `android/local.properties`, although the other commands were executed before it so it could have been a cumulative result? Double-check this next time. 
