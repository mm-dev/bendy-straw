# Test:

Rename a playlist:
  Does 'export" button become enabled?
  Scroll off screen and back, does new name persist?
  Switch to new tab and back, does new name persist?
  Export db, re-import, does new name persist?

### Remote playlists

  Open expandable tile
  tap link - does it open
  delete a playlist
  copy a playlist to another db
  move playlist to another db
  Does 'export" button become enabled during all of the above?
  Save db, re-open, have changes persisted?


### Channel subs

  Open expandable tile
  tap link - does it open
  delete a channel
  copy a channel to another db
  move channel to another db
  Does 'export" button become enabled during all of the above?
  Save db, re-open, have changes persisted?

### Custom playlists

  Open expandable tile
  tap link - does it open
  delete a stream
  copy a stream to another playlist
  move stream to another playlist
  Does 'export" button become enabled during all of the above?
  copy playlist to anther db
  move playlist to anther db
  delete playlist
  export playlist as text - check it in filesystem
  Save db, re-open, have changes persisted?




# README

Features
- Copy items from one playlist to another
- Delete items from a playlist
- Copy a playlist into another database file (eg NewPipe.zip)
- Delete playlists from a database
- Open multiple databases at once in tabs (eg if you have exports from multiple devices and want to combine playlists - this was my motivation to build the app as I wanted to do this myself as I had different saved items on all my devices and could never find which dievice I needed to find an item)
- Streams (audio/video) can be opened directly from BendyStraw, as the URLs are clickable. If you set video links to open in NewPipe you can make a spli-screen view and jump around your playlists
- Export a playlist as plain text URLs, which for example can be used with yt-dlp to batch-download the entire playlist *NB I originally built an export to .M3U option, but found that youtube links in .M3U playlists don't work any more, at least on my device in players such as VLC. If anybody has any further info on this or sees another reason to bring back the .M3U exports, or other playlist foprmats, let me know and I'll look into it* 


Tips


Explanation especially for A13 about permissions
On my Tab S8 there is a long delay after clicing to give permission where nothing seems to be happening, this may be because I'm rooted
tabs scroll horizontally





# Future/Roadmap

Easy way to click subscribe/like on videos to help channels?

Full database merge
- Combine channel subscriptions
- Combine remote playlists
- Local playlists:
  - If playlist with same name exists, combine them
  - Otherwise copy entire playlist



# Bugs



# Todo

- Intall LibreTube, give it a go, see how it works, find out about its DB
- Add new button/functionality 'Reverse current playlist order'
- Add new column 'upload date'
  - Allow sorting by this column
  - Add preference for showing/hiding columns
    - Length
    - upload date
    - Channel
  - Default to showing columns same as current version
- Add new button/funtionality 'Import JSON'
  - Write JSON spec/example
- Add proper documentation









tables should collapse to minimum size if not fullof rows

Open multiple files possible? **Shelve it for now, too many potential problems with file pickers**


" first item should always be selected
first item should always be selected copy/move playlist too





# Done
Array of chosen colour themes instead of comletely random
Move close button to tab
Align tabs to left
'Custom playlists'heading alignment need thought, capitalise as title

Open 'Channels' Select all deselect 1 delte items
Crash: 
SQL ERROR:
Bad state: This database has already been closed

Same happens if 2 are deselected/remaining... seems related to large number of deletes at once?

File manager bar wastes too much space
'Dismiss' button on dialog shoulld be less ambigous, close button in topright corner would be best

Base info layer still visible underneath tabs
  background color behind tabmanager and filemanager might be easiest fix


editable text not obvious enough, just use border if easiest


export playlist dialog clickable link to yt-dlp

table URLs clickable
Make difference between table/tab bg colour less contrast
Copying selected items between tables, themes not used correctly
LOGO
Colour of 'edited text' asterisk is bad
Could maybe invert text when edited instead?

make editable text submit on click outside (don't require enter)
right padding looks bad
Make editable playlist names bold as look thin on android
editable text match height to button  height
Repeated firing of aDatabaseWasDirtied
BUG 'selected items' button text is invisible
Android 13 youtube links dont open, no error
BUG Android 13 every time new file is opened, opening permissions again
tab [X] is too low/out of line on Android
Checkbox/row alignment off on android
'selected items' copy dialog, db should colapse to minimum size if not filled with playlist entries
- playlist buttons oveflow - wrap them and/or make padding smaller
- hard to scroll destinationlists, need some padding at the side for finger to grab, try making scrollnars visible too as not obvious you can scroll
- destinationlists, click a destination, botton grows too big and layout goes bad
- can't see playlist name, so reduce padding between bttons on low width screen
base theme illegible text!
- 'selected item' buttons need compact versions, spilling out on mobile
- 'copy confim' button still spilling out on mobile
- 'delete playlist' dismiss (x) button still spilling out on mobile

Size (eg orientation, window resize) change causes dialog to redraw

Column sorting
Can re-write just join_index, db part is easy so go for it
Preferences, back button wrong colour

Compact version of playlist buttons could do with a little padding around icons
Should probably rebuild main.dart when preferences change, to make sure every widget catches up
Need to set defaults and use them everywhere needed
Can icon/count be moved to right of expandingtile on compact layout?
Selected items 'copy` dialog has no close button

BUG on copy playlist popup, no need for min height, plus it ovrspills on split screen on tab S4
base info wrong text size (shrinking to fit in container) and # icons need to be aligned better. text should be shortened/simlified and padding reduced on compact screens

base info overflows on note3 landscape

WHen opening zip if all tables are missing, add prefix to error message sayiung "probably not a newpipe zip"
Try harder to remove row checkboxes all the way to left, with more padding on right

landcape layout standard
  custom playlist header stuckl to top give it some padding
Open db
Open 2nd db
Close 1 db
open 2nd db again -- has same color

tab clos button can get lost under settings icon

