// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'constants.dart';
import 'screen.dart';

/// A table cell specialised for showing a compact number value.
///
/// Used for eg channel subscriber counts, the number is shown in a compact,
/// human-friendly format, so eg `1,220,123` becomes `1.2M` and so on.
class TableCellNumericCompact extends StatelessWidget {
  final dynamic value;
  const TableCellNumericCompact({
    super.key,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    Map cellLayout = Screen.isCompact(context)
        ? BS.tableCellLayoutCompact
        : BS.tableCellLayoutStandard;
    return Padding(
      padding: cellLayout['padding'],
      child: Text(NumberFormat.compact().format(value),
          style: cellLayout['textStyle']),
    );
  }
}
