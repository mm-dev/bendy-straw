// SPDX-License-Identifier: GPL-3.0-only

import 'dart:convert';

import 'package:flutter/material.dart';

import 'constants.dart';
import 'copy_move_confirm_button.dart';
import 'db_manager.dart';
import 'destination_chooser_header.dart';
import 'destination_list_items.dart';
import 'enum.dart';
import 'screen.dart';

/// Show a dialog to choose a destination for copied/moved items.
class DestinationChooser extends StatelessWidget {
  final String tableIdFrom;
  final String dbPathFrom;
  final TableType tableTypeFrom;
  final bool isMoveMode;
  final Function chosenDestinationJsonSetter;
  final Function chosenDestinationJsonGetter;
  final Function moveModeSetter;
  final Function copyFunction;
  final String chosenDestinationJson;
  final int? itemCount;
  final bool isFullTableCopy;

  const DestinationChooser({
    super.key,
    required this.tableIdFrom,
    required this.dbPathFrom,
    required this.tableTypeFrom,
    required this.isMoveMode,
    required this.moveModeSetter,
    required this.chosenDestinationJson,
    required this.chosenDestinationJsonSetter,
    required this.chosenDestinationJsonGetter,
    required this.copyFunction,
    this.itemCount,
    this.isFullTableCopy = false,
  });

  @override
  Widget build(BuildContext context) {
    final Map dialogLayout = Screen.isCompact(context)
        ? BS.dialogLayoutCompact
        : BS.dialogLayoutStandard;

    List<String> validForCopyDbPaths = _getValidDestinationDbPaths();

    List simpleDbsAndPlaylistsInfo =
        _getSimpleDbsAndPlaylistsInfo(validForCopyDbPaths);

    return SizedBox(
      width: Screen.width(context) * dialogLayout['screenWidthPercent'],
      height: Screen.height(context) * dialogLayout['screenHeightPercent'],
      child: Padding(
        padding: EdgeInsets.only(
          top: dialogLayout['topPad'],
          right: dialogLayout['rightPad'],
          bottom: dialogLayout['bottomPad'],
          left: dialogLayout['leftPad'],
        ),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          // Title
          DestinationChooserHeader(
              validForCopyDbPaths: validForCopyDbPaths,
              tableType: tableTypeFrom,
              itemCount: itemCount,
              isMoveMode: isMoveMode),

          // List of Databases
          validForCopyDbPaths.isEmpty
              ? _getNeedMoreDatabasesText()
              : Expanded(
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverList(
                          delegate: SliverChildListDelegate(<Widget>[
                        Align(
                          alignment: Alignment.topCenter,
                          child: Wrap(
                              alignment: WrapAlignment.start,
                              spacing: BS.paddingMedium,
                              runSpacing: BS.paddingMedium,
                              children: [
                                ...simpleDbsAndPlaylistsInfo
                                    .map((dbAndPlaylists) {
                                  ///
                                  /// Loop through Databases
                                  String dbPath = dbAndPlaylists['dbPath'];
                                  return Container(
                                    padding: const EdgeInsets.only(
                                        left: BS.paddingMedium,
                                        right: BS.paddingMedium),
                                    constraints: BoxConstraints.loose(Size(
                                        BS.destinationListWidth,
                                        _isSelectedItemsCopy()
                                            ? BS.destinationListHeight
                                            : BS.destinationSingleItemHeight)),
                                    child: DestinationListItems(
                                        tableId: tableIdFrom,
                                        dbPath: dbPathFrom,
                                        tableType: tableTypeFrom,
                                        destinationDbPath: dbPath,
                                        chosenDestinationJsonGetter:
                                            chosenDestinationJsonGetter,
                                        chosenDestinationJsonSetter:
                                            chosenDestinationJsonSetter,
                                        localPlaylists: isFullTableCopy
                                            ? []
                                            : dbAndPlaylists['localPlaylists']),
                                  );
                                })
                              ]),
                        ),
                      ])),
                    ],
                  ),
                ),

          // Move mode checkbox and confirm button
          if (validForCopyDbPaths.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(top: BS.paddingLarge),
              child: CopyMoveConfirmButton(
                buttonLabel: _getActionDescription(
                    context: context,
                    destinationJson: chosenDestinationJson,
                    isMoveMode: isMoveMode),
                isMoveMode: isMoveMode,
                onMoveModeChanged: (bool? value) {
                  moveModeSetter(value);
                },
                onConfirmPressedCallback: chosenDestinationJson.isEmpty
                    ? null
                    : () {
                        Map playlistPathMap = jsonDecode(chosenDestinationJson);
                        if (_isSelectedItemsCopy()) {
                          copyFunction(
                              dbPathFrom: dbPathFrom,
                              dbPathTo: playlistPathMap['dbPath'],
                              playlistUidFrom: tableIdFrom,
                              playlistUidTo: playlistPathMap['playlistId'],
                              isMoveMode: isMoveMode);
                        } else {
                          copyFunction(
                              dbPathFrom: dbPathFrom,
                              dbPathTo: playlistPathMap['dbPath'],
                              playlistUidFrom: tableIdFrom,
                              isMoveMode: isMoveMode);
                        }
                        Navigator.of(context).pop();
                      },
              ),
            )
        ]),
      ),
    );
  }

  /// Get a list of paths that we can copy to, omitting the current table.
  List<String> _getValidDestinationDbPaths() {
    List<String> paths;

    /// If this is a 'selected items' copy, we need to show the current
    /// database as well as the other opened databases, as items can be copied
    /// from one playlist to another.
    if (_isSelectedItemsCopy()) {
      paths = DbManager.databases.keys.toList();
    } else {
      paths = DbManager.databases.keys
          .where((dbPath) => dbPath != dbPathFrom)
          .toList();
    }
    return paths;
  }

  /// Create simple list of databases/playlists to work from, containing only
  /// the info we need to draw the radio buttons.
  List<Map> _getSimpleDbsAndPlaylistsInfo(List<String> dbPaths) {
    return dbPaths
        .map((dbPath) => {
              'dbPath': dbPath,
              'localPlaylists': DbManager.getLocalPlaylists(dbPath).keys.map(
                  (playlistId) => {
                        'id': playlistId,
                        'name':
                            DbManager.getLocalPlaylistName(dbPath, playlistId)
                      }),
            })
        .toList();
  }

  /// Get a description for the action in its current state, to be displayed
  /// with the [CopyMoveConfirmButton].
  String _getActionDescription(
      {required BuildContext context,
      required String destinationJson,
      required bool isMoveMode}) {
    String itemDescription = itemCount == null
        ? 'playlist'
        : '${itemCount} ${itemCount == 1 ? 'item' : 'items'}';
    String description = '';
    String shortDescription = '';
    if (destinationJson.isNotEmpty) {
      Map playlistPathMap = jsonDecode(destinationJson);
      String dbPath = playlistPathMap['dbPath'] ?? '';
      String playlistId = playlistPathMap['playlistId'] ?? '';

      String pathDescription;
      if (tableTypeFrom == TableType.localPlaylist) {
        if (isFullTableCopy) {
          pathDescription = DbManager.getPrettyName(dbPath);
        } else {
          pathDescription =
              "'${DbManager.getLocalPlaylistName(dbPath, playlistId)}' in ${DbManager.getPrettyName(dbPath)}";
        }
      } else {
        // If it's not a custom playlist we know it must be one of the other
        // known tables.
        pathDescription =
            "'${BS.knownTables[tableIdFrom]?['displayName']}' in ${DbManager.getPrettyName(dbPath)}";
      }
      description =
          '${isMoveMode ? 'Move' : 'Copy'} $itemDescription to $pathDescription';
      shortDescription = isMoveMode ? 'Move' : 'Copy';
    }
    return Screen.isCompact(context) ? shortDescription : description;
  }

  /// Get whether the current action is a 'selected items copy', ie a copy/move
  /// of items which have been checked, as opposed to copying of an entire
  /// playlist.
  bool _isSelectedItemsCopy() {
    return tableTypeFrom == TableType.localPlaylist && !isFullTableCopy;
  }

  /// Get a contextually-customised string explaining that the current action
  /// is only possible when multiple databases are opened.
  Widget _getNeedMoreDatabasesText() {
    String itemDescription = isFullTableCopy
        ? 'an entire playlist'
        : BS.knownTables[tableIdFrom]?['displayName'];
    return Text('''
To copy $itemDescription you need 2 or more databases open at the same time.

Open another database file.
''');
  }
}
