// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:sqlite3/sqlite3.dart' as sqlite3;

import 'constants.dart';
import 'enum.dart';
import 'screen.dart';

/// An icon indicating the type of playlist, and a number indicating how many
/// items it contains.
class PlaylistIconAndItemCount extends StatelessWidget {
  const PlaylistIconAndItemCount({
    super.key,
    required this.tableType,
    required this.resultSet,
  });

  final TableType tableType;
  final sqlite3.ResultSet resultSet;

  @override
  Widget build(BuildContext context) {
    bool screenIsCompact = Screen.isCompact(context);
    CrossAxisAlignment localPlaylistCrossAxisAlignment =
        screenIsCompact ? CrossAxisAlignment.start : CrossAxisAlignment.center;
    Alignment localPlaylistAlignment =
        screenIsCompact ? Alignment.topRight : Alignment.centerRight;
    double localPlaylistItemCountTopPadding =
        screenIsCompact ? BS.paddingSmall : 0;
    double localPlaylistItemCountTextHeight = screenIsCompact ? 0.9 : 1;
    return Row(
        crossAxisAlignment: tableType == TableType.localPlaylist
            ? localPlaylistCrossAxisAlignment
            : CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
              child: Padding(
            padding: const EdgeInsets.only(right: BS.paddingSmall),
            child: SizedBox(
                width: BS.playlistItemCountWidth,
                child: Align(
                    alignment: tableType == TableType.localPlaylist
                        ? localPlaylistAlignment
                        : Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: tableType == TableType.localPlaylist
                              ? localPlaylistItemCountTopPadding
                              : 0),
                      child: Text(
                        resultSet.length.toString(),
                        style: TextStyle(
                            height: localPlaylistItemCountTextHeight,
                            leadingDistribution: TextLeadingDistribution.even,
                            fontSize: BS.fontSizePlaylistItemCount),
                      ),
                    ))),
          )),
          Flexible(
            child: switch (tableType) {
              TableType.remotePlaylists =>
                const Icon(Icons.video_library_rounded),
              TableType.subscriptions =>
                const Icon(Icons.recent_actors_rounded),
              TableType.localPlaylist => const Icon(Icons.list),
            },
          ),
        ]);
  }
}
