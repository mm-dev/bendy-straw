// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'app_state.dart';
import 'constants.dart';
import 'db_manager.dart';
import 'destination_chooser.dart';
import 'enum.dart';
import 'screen.dart';
import 'tooltip_icon_button.dart';

/// Buttons used to perform actions based on currently-selected items.
///
/// Depending on the type of widget the selection can be:
/// - Streams within a playlist
/// - Channels within the 'channel subscriptions' widget
class SelectedItemsButtonsWidget extends StatefulWidget {
  final String tableId;
  final String tableDbPath;
  final TableType tableType;

  const SelectedItemsButtonsWidget({
    super.key,
    required this.tableId,
    required this.tableDbPath,
    required this.tableType,
  });

  @override
  State<SelectedItemsButtonsWidget> createState() =>
      _SelectedItemsButtonsWidgetState();
}

class _SelectedItemsButtonsWidgetState
    extends AppState<SelectedItemsButtonsWidget> {
  @override
  late List<String>? listenForChanges = switch (widget.tableType) {
    TableType.localPlaylist => ['localPlaylistSelectedRows'],
    TableType.remotePlaylists => ['remotePlaylistsSelectedRows'],
    TableType.subscriptions => ['subscriptionsSelectedRows'],
  };

  List<String>? _selectedInThisTable;
  String _chosenDestinationJson = '';

  @override
  Widget build(BuildContext context) {
    _selectedInThisTable = switch (widget.tableType) {
      TableType.localPlaylist =>
        AppState.get('localPlaylistSelectedRows')?[widget.tableDbPath]
            ?[widget.tableId],
      TableType.remotePlaylists =>
        AppState.get('remotePlaylistsSelectedRows')?[widget.tableDbPath]
            ?[widget.tableId],
      TableType.subscriptions =>
        AppState.get('subscriptionsSelectedRows')?[widget.tableDbPath]
            ?[widget.tableId],
    };

    bool screenIsCompact = Screen.isCompact(context);

    return Container(
      alignment: Alignment.centerLeft,
      child: Wrap(spacing: BS.paddingMedium, children: [
        TooltipIconButton(
            text: screenIsCompact ? 'Copy' : 'Copy selected to',
            icon: const Icon(Icons.copy_rounded),
            layout: TooltipIconButtonLayout.iconOnLeft,
            onPressedCallback:
                _selectedInThisTable != null && _selectedInThisTable!.isNotEmpty
                    ? () {
                        _confirmCopySelectedItems(context: context);
                      }
                    : null),
        TooltipIconButton(
            text: screenIsCompact ? 'Delete' : 'Delete selected',
            icon: const Icon(Icons.delete_rounded),
            layout: TooltipIconButtonLayout.iconOnLeft,
            onPressedCallback:
                _selectedInThisTable != null && _selectedInThisTable!.isNotEmpty
                    ? _deleteSelectedItems
                    : null),
      ]),
    );
  }

  /// Start the copy/move process by displaying a dialog with a confirm button,
  /// which will copy or move the items when pressed.
  Future<void> _confirmCopySelectedItems(
      {required BuildContext context, bool isMoveMode = false}) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return Center(
            child: SimpleDialog(children: [
              DestinationChooser(
                  tableIdFrom: widget.tableId,
                  dbPathFrom: widget.tableDbPath,
                  tableTypeFrom: widget.tableType,
                  itemCount: _selectedInThisTable!.length,
                  //
                  isMoveMode: isMoveMode,
                  moveModeSetter: (bool? value) {
                    setState(() {
                      isMoveMode = value ?? false;
                    });
                  },
                  //
                  chosenDestinationJson: _chosenDestinationJson,
                  chosenDestinationJsonSetter: (String str) {
                    setState(() {
                      _chosenDestinationJson = str;
                    });
                  },
                  chosenDestinationJsonGetter: () {
                    return _chosenDestinationJson;
                  },
                  //
                  copyFunction: _copySelectedItems)
            ]),
          );
        });
      },
    );
  }

  /// Update the database to copy the selected items to the new destination.
  void _copySelectedItems(
      {required String dbPathFrom,
      required String dbPathTo,
      String? playlistUidFrom,
      String? playlistUidTo,
      bool isMoveMode = false}) {
    print('Copy $dbPathFrom, $playlistUidFrom to $dbPathTo, $playlistUidTo');

    switch (widget.tableType) {
      case TableType.localPlaylist:
        if (playlistUidFrom != null && playlistUidTo != null) {
          DbManager.copyItemsFromLocalPlaylist(dbPathFrom, dbPathTo,
              playlistUidFrom, playlistUidTo, _selectedInThisTable, isMoveMode);
        }
      case TableType.remotePlaylists:
        DbManager.copyItemsFromRemotePlaylists(
            dbPathFrom, dbPathTo, _selectedInThisTable, isMoveMode);
      case TableType.subscriptions:
        DbManager.copyItemsFromChannelSubscriptions(
            dbPathFrom, dbPathTo, _selectedInThisTable, isMoveMode);
    }

    // Force rebuild of widgets to reflect changes.
    AppState.update(dbPathFrom, DbManager.databases, forceRebuild: true);
    AppState.update(dbPathTo, DbManager.databases, forceRebuild: true);
  }

  /// Update the database to delete the selected items.
  void _deleteSelectedItems() {
    print('Deleting: $_selectedInThisTable');
    switch (widget.tableType) {
      case TableType.localPlaylist:
        DbManager.deleteItemsFromLocalPlaylist(
            widget.tableDbPath, widget.tableId, _selectedInThisTable);
      case TableType.remotePlaylists:
        DbManager.deleteItemsFromRemotePlaylists(
            widget.tableDbPath, _selectedInThisTable);
      case TableType.subscriptions:
        DbManager.deleteItemsFromChannelSubscriptions(
            widget.tableDbPath, _selectedInThisTable);
    }

    // Force rebuild of widgets to reflect changes.
    AppState.update(widget.tableDbPath, DbManager.databases,
        forceRebuild: true);
  }
}
