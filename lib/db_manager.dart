// SPDX-License-Identifier: GPL-3.0-only

import 'dart:io';

import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:sqlite3/sqlite3.dart';

import 'app_state.dart';
import 'constants.dart';
import 'sql_helpers.dart';
import 'sql_local_playlists.dart';
import 'sql_remote_playlists.dart';
import 'sql_subscriptions.dart';

/// Handle all database management tasks.
///
/// [_databases] is a [Map] containing info for all imported databases. Each is
/// stored in that map under its key ([dbPath]), which is a filesystem path to
/// the database as extracted from its containing `zip`. Throughout the app,
/// [dbPath] is used whenever we want to specify which database we are dealing
/// with for an operation.
class DbManager {
  static final Map<String, Map> _databases = {};

  static Map<String, Map> get databases {
    return _databases;
  }

  static int get numberOfDatabases {
    return _databases.length;
  }

  /// Using [dbPath] as a key, store some info about a database in a [Map].
  /// Return `true` if a change is made, otherwise `false`.
  static bool addDbByPathKey(String dbPath, String originalZipPath) {
    bool didChange = false;

    if (!_databases.containsKey(dbPath) && SqlHelpers.isCompatibleDatabase(dbPath)) {
      /// Count to ensure consecutive tabs never end up with the same colour
      int totalDbsOpenedSinceSessionStart = AppState.get('totalDbsOpenedSinceSessionStart') ?? 0;

      FlexScheme flexScheme =
          BS.flexSchemeColors[totalDbsOpenedSinceSessionStart % BS.flexSchemeColors.length];
      bool darkIsTrueBlack = AppState.get('preferences')?.getBool('darkIsTrueBlack') ??
          BS.defaultPreferences['darkIsTrueBlack'];

      _databases[dbPath] = {
        'themeData': FlexThemeData.light(
          scheme: flexScheme,
          visualDensity: VisualDensity.standard,
        ),
        'themeDataDark': FlexThemeData.dark(
          scheme: flexScheme,
          darkIsTrueBlack: darkIsTrueBlack,
          visualDensity: VisualDensity.standard,
        ),
        'hasUnsavedChanges': false,
        'prettyName': originalZipPath.split('/').last,
        'originalDirectoryPath': originalZipPath.replaceFirst(originalZipPath.split('/').last, ''),
        'originalZipPath': originalZipPath,
        'localPlaylists': SqlLocalPlaylists.getLocalPlaylistsMap(dbPath),
      };

      AppState.update('totalDbsOpenedSinceSessionStart', totalDbsOpenedSinceSessionStart + 1);
      didChange = true;
    }
    return didChange;
  }

  /// Using [dbPath] as a key, remove info about a database.
  /// Return `true` if a change is made, otherwise `false`.
  static bool removeDbByPathKey(String dbPath) {
    bool didChange = false;
    if (_databases.containsKey(dbPath)) {
      _databases.remove(dbPath);
      didChange = true;
    }
    return didChange;
  }

  /// Return a [Map] containing data for each of the custom playlists.
  static Map<String, Map> getLocalPlaylists(String dbPath) {
    Map dbInfo = _databases[dbPath]!;
    return dbInfo['localPlaylists'];
  }

  /// Return the display name of a local playlist.
  static String getLocalPlaylistName(String dbPath, String playlistUid) {
    return _databases[dbPath]?['localPlaylists'][playlistUid]['displayName'] ??
        'Playlist name not found';
  }

  /// Return the display name assigned to a database.
  static String getPrettyName(String dbPath) {
    return _databases[dbPath]?['prettyName'] ?? '';
  }

  /// Return the directory path a playlist was originally opened from.
  static String getOriginalDirectoryPath(String dbPath) {
    return _databases[dbPath]?['originalDirectoryPath'] ?? 'Original directory path not found';
  }

  /// Return the current theme assigned to a database, based on current system
  /// brightness or an overriding prefrerence variable.
  static ThemeData getDbCurrentThemeData(String dbPath, BuildContext context) {
    String brightnessOverride = AppState.get('preferences')?.getString('brightnessOverride') ??
        BS.defaultPreferences['brightnessOverride'];
    Brightness systemBrightness = MediaQuery.of(context).platformBrightness;
    ThemeData themeData = (brightnessOverride == 'dark' ||
            (brightnessOverride == 'system' && systemBrightness == Brightness.dark))
        ? _databases[dbPath]!['themeDataDark']
        : _databases[dbPath]!['themeData'];

    return themeData;
  }

  static bool getDbDirtyState(String dbPath) {
    return _databases[dbPath]?['hasUnsavedChanges'];
  }

  // Allows 'save' button to activate/deactivate
  static void setDbDirtyState(String dbPath, bool isDirty) {
    _databases[dbPath]?['hasUnsavedChanges'] = isDirty;
    // `aDatabaseWasDirtied`: it doesn't matter what the value is as we just
    // want the message to be broadcast so listening widgets redraw, pulling in
    // whichever fresh data they need.
    AppState.update('aDatabaseWasDirtied', null, forceRebuild: true);
  }

  /// Wrapper just to avoid classes other than this calling [SqlHelpers]
  /// functions directly.
  ///
  /// [columns] is a string describing columns, ready to be passed into an SQL
  /// query, eg:
  /// ```
  /// "col1Name AS col1DisplayName, col2Name AS col2DisplayName"
  /// ```
  static ResultSet getFilteredColumns(String dbPath, String tableId, String columns) {
    return SqlHelpers.getFilteredColumns(dbPath, tableId, columns);
  }

  ///////////////////////
  ///
  /// Local/Custom Playlists
  ///
  ///////////////////////

  static void copyLocalPlaylist(
      String dbPathFrom, String dbPathTo, String playlistUid, bool isMoveMode) {
    print('copyLocalPlaylist(): $dbPathFrom, $dbPathTo, $playlistUid, $isMoveMode');
    SqlLocalPlaylists.copyLocalPlaylist(dbPathFrom, dbPathTo, playlistUid);
    rebuildLocalPlaylistsMap(dbPathTo);
    setDbDirtyState(dbPathTo, true);

    if (isMoveMode) {
      deleteLocalPlaylist(dbPathFrom, playlistUid);
    }
  }

  static void deleteLocalPlaylist(String dbPath, String playlistUid) {
    print('deleteLocalPlaylist(): $dbPath, $playlistUid');
    SqlLocalPlaylists.deleteLocalPlaylist(dbPath, playlistUid);
    rebuildLocalPlaylistsMap(dbPath);
    setDbDirtyState(dbPath, true);
  }

  static void renameLocalPlaylist(String dbPath, String playlistUid, String newName) {
    SqlLocalPlaylists.renameLocalPlaylist(dbPath, playlistUid, newName);
    rebuildLocalPlaylistsMap(dbPath);
    setDbDirtyState(dbPath, true);
  }

  static void rebuildLocalPlaylistsMap(String dbPath) {
    Map dbInfo = _databases[dbPath]!;
    dbInfo['localPlaylists'] = SqlLocalPlaylists.getLocalPlaylistsMap(dbPath);
  }

  static void importPlaylistFromJson(String dbPath, String playlistName, List<dynamic> streams) {
    print('importPlaylistFromJson(): $dbPath, $playlistName');
    SqlLocalPlaylists.importPlaylistFromJson(dbPath, playlistName, streams);

    rebuildLocalPlaylistsMap(dbPath);
    AppState.update(dbPath, DbManager.databases, forceRebuild: true);
    //setDbDirtyState(dbPath, true);
  }

  static void reverseLocalPlaylistOrder(String dbPath, String playlistUid) {
    print('reverseLocalPlaylistOrder(): $dbPath, $playlistUid');
    SqlLocalPlaylists.reverseLocalPlaylistOrder(dbPath, playlistUid);

    rebuildLocalPlaylistsMap(dbPath);
    setDbDirtyState(dbPath, true);
  }

  static void shuffleLocalPlaylistOrder(String dbPath, String playlistUid) {
    print('shuffleLocalPlaylistOrder(): $dbPath, $playlistUid');
    SqlLocalPlaylists.shuffleLocalPlaylistOrder(dbPath, playlistUid);

    rebuildLocalPlaylistsMap(dbPath);
    setDbDirtyState(dbPath, true);
  }

  static void copyItemsFromLocalPlaylist(String dbPathFrom, String dbPathTo, String playlistUidFrom,
      String playlistUidTo, List<String>? streamUids, bool isMoveMode) {
    print(
        'copyItemsFromLocalPlaylist(): $dbPathFrom, $dbPathTo, $playlistUidFrom, $playlistUidTo, $streamUids, $isMoveMode');

    SqlLocalPlaylists.copyItemsFromLocalPlaylist(
        dbPathFrom, dbPathTo, playlistUidFrom, playlistUidTo, streamUids);
    rebuildLocalPlaylistsMap(dbPathTo);
    setDbDirtyState(dbPathTo, true);

    if (isMoveMode) {
      deleteItemsFromLocalPlaylist(dbPathFrom, playlistUidFrom, streamUids);
    }
  }

  static void deleteItemsFromLocalPlaylist(
      String dbPath, String playlistUid, List<String>? streamUids) {
    print('deleteItemsFromLocalPlaylist(): $dbPath, $playlistUid, $streamUids');

    if (streamUids != null) {
      SqlLocalPlaylists.deleteItemsFromLocalPlaylist(dbPath, playlistUid, streamUids);
      rebuildLocalPlaylistsMap(dbPath);
      setDbDirtyState(dbPath, true);
    }
  }

  /// Get a path to which a text playlist will be exported.
  static String getExportLocalPlaylistFilepath(String dbPath, String playlistUid) {
    Map? playlistData = getLocalPlaylists(dbPath)[playlistUid];
    String playlistDisplayName = playlistData?['displayName'];

    /// Create/sanitise filepath
    String fileName = getOriginalDirectoryPath(dbPath) +
        playlistDisplayName.replaceAll(RegExp(r'[^a-zA-Z0-9]'), '-');
    fileName += '.txt';
    return fileName;
  }

  /// Export a list of URLs from a playlist.
  ///
  /// The exported file can be used with `yt-dlp` to batch download the
  /// playlist.
  static void exportLocalPlaylistToYtDlp(String dbPath, String playlistUid) {
    print('exportLocalPlaylistToM3u(): $dbPath, $playlistUid');

    Map? playlistData = getLocalPlaylists(dbPath)[playlistUid];
    String playlistDisplayName = playlistData?['displayName'];
    ResultSet playlistRowsResultSet = playlistData?['rows'];

    String fileName = getExportLocalPlaylistFilepath(dbPath, playlistUid);

    // Comments header
    String fileContents = '# For use with yt-dlp to batch download eg:\n';
    fileContents += '# `yt-dlp --batch-file "$fileName"`\n\n\n';

    // Playlist name comment
    fileContents += '# Playlist Title: $playlistDisplayName\n\n\n';

    // URLs with title as a leading comment
    for (final row in playlistRowsResultSet) {
      fileContents += '# ${row['Title']} | ${row['Channel']}\n';
      fileContents += '${row['URL']}\n\n';
    }
    print(fileContents);

    // Write the file
    final file = File(fileName);
    file.writeAsString(fileContents);
  }

  static List<String> getLocalPlaylistAsYouTubeIdList(String dbPath, String playlistUid) {
    print('getLocalPlaylistAsYouTubeIdList(): $dbPath, $playlistUid');

    Map? playlistData = getLocalPlaylists(dbPath)[playlistUid];
    ResultSet playlistRowsResultSet = playlistData?['rows'];

    RegExp exp = RegExp(r'v=(.*$)');
    return playlistRowsResultSet.map((row) => exp.firstMatch(row['URL'])![1] ?? '').toList();
  }

  static void sortLocalPlaylistByColumn(
      {required String dbPath,
      required String playlistUid,
      required int sortColumnIndex,
      required int streamUidColumnIndex,
      required bool isNumeric,
      required bool ascending}) {
    print('streamUidColumnIndex: $streamUidColumnIndex');

    List<List<Object?>> playlistRows = getLocalPlaylists(dbPath)[playlistUid]?['rows'].rows;

    // As everything is passed by reference, [sort] here actually changes the order of the streams directly, but only in the cached version... so it will look right in the UI, but the sqllite database isn't affected yet.
    if (isNumeric) {
      if (ascending) {
        // Ascending (a, b)
        playlistRows.sort((a, b) => int.parse(a[sortColumnIndex].toString())
            .compareTo(int.parse(b[sortColumnIndex].toString())));
      } else {
        // Descending (b, a)
        playlistRows.sort((b, a) => int.parse(a[sortColumnIndex].toString())
            .compareTo(int.parse(b[sortColumnIndex].toString())));
      }
    } else {
      // Alphabetic column
      if (ascending) {
        playlistRows.sort((a, b) => a[sortColumnIndex]
            .toString()
            .toLowerCase()
            .compareTo(b[sortColumnIndex].toString().toLowerCase()));
      } else {
        playlistRows.sort((b, a) => a[sortColumnIndex]
            .toString()
            .toLowerCase()
            .compareTo(b[sortColumnIndex].toString().toLowerCase()));
      }
    }

    //print(playlistRows);
    SqlLocalPlaylists.reOrderLocalPlaylist(
        dbPath,
        playlistUid,

        /// make a list of streamUids in the new order
        playlistRows.map((row) => row[streamUidColumnIndex].toString()).toList());

    setDbDirtyState(dbPath, true);
  }

  ///////////////////////
  ///
  /// Remote/Bookmarked Playlists
  ///
  ///////////////////////

  static void copyItemsFromRemotePlaylists(
      String dbPathFrom, String dbPathTo, List<String>? playlistUids, bool isMoveMode) {
    print('copyItemsFromRemotePlaylists(): $dbPathFrom, $dbPathTo, $playlistUids, $isMoveMode');

    SqlRemotePlaylists.copyItemsFromRemotePlaylists(dbPathFrom, dbPathTo, playlistUids);
    rebuildLocalPlaylistsMap(dbPathTo);
    setDbDirtyState(dbPathTo, true);

    if (isMoveMode) {
      deleteItemsFromRemotePlaylists(dbPathFrom, playlistUids);
    }
  }

  static void deleteItemsFromRemotePlaylists(String dbPath, List<String>? playlistUids) {
    print('deleteItemsFromRemotePlaylists(): $dbPath, $playlistUids');

    if (playlistUids != null) {
      SqlRemotePlaylists.deleteItemsFromRemotePlaylists(dbPath, playlistUids);
      rebuildLocalPlaylistsMap(dbPath);
      setDbDirtyState(dbPath, true);
    }
  }

  ///////////////////////
  ///
  /// Channel Subscriptions
  ///
  ///////////////////////

  static void copyItemsFromChannelSubscriptions(
      String dbPathFrom, String dbPathTo, List<String>? subscriptionUids, bool isMoveMode) {
    print(
        'copyItemsFromChannelSubscriptions(): $dbPathFrom, $dbPathTo, $subscriptionUids, $isMoveMode');

    SqlChannelSubscriptions.copyItemsFromChannelSubscriptions(
        dbPathFrom, dbPathTo, subscriptionUids);
    rebuildLocalPlaylistsMap(dbPathTo);
    setDbDirtyState(dbPathTo, true);

    if (isMoveMode) {
      deleteItemsFromChannelSubscriptions(dbPathFrom, subscriptionUids);
    }
  }

  static void deleteItemsFromChannelSubscriptions(String dbPath, List<String>? subscriptionUids) {
    print('deleteItemsFromChannelSubscriptions(): $dbPath, $subscriptionUids');
    if (subscriptionUids != null) {
      SqlChannelSubscriptions.deleteItemsFromChannelSubscriptions(dbPath, subscriptionUids);
      rebuildLocalPlaylistsMap(dbPath);
      setDbDirtyState(dbPath, true);
    }
  }
}
