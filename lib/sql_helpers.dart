// SPDX-License-Identifier: GPL-3.0-only

import 'package:sqlite3/sqlite3.dart';

import 'app_state.dart';
import 'constants.dart';

/// Class with static functions to perform some generic tasks on the database.
class SqlHelpers {
  /// Wrapper to be used around all sqlite calls eg `select`, `execute`.
  static void tryAndCatchErrors(function) {
    try {
      function();
    } on Exception catch (e) {
      AppState.debug('SQL EXCEPTION:\n$e', alwaysOnScreen: true);
    } catch (e) {
      AppState.debug('SQL ERROR:\n$e', alwaysOnScreen: true);
    }
  }

  /// Escape single quotes in a string by converting them to two single quotes.
  static String escapeQuotes(str) {
    str ??= '';
    str = str.toString();
    return str.replaceAll("'", "''");
  }

  /// Check if a database file is a NewPipe database in a recognised format.
  /// If not valid, display schema as it may be useful for improving
  /// compatibility with old dbs.
  static bool isCompatibleDatabase(dbPath) {
    bool isCompatible = false;

    tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      isCompatible = true;
      List<String> missingTables = [];

      /// Check that the tables we need exist in the database.
      /// The query returns a resultset of length 1 if the table exists,
      /// otherwise 0.
      for (final tableName in BS.requiredTables) {
        if (db.select('''
          SELECT name FROM sqlite_master 
          WHERE type='table' AND name='${tableName}';
        ''').isEmpty) {
          missingTables.add(tableName);
          isCompatible = false;
        }
      }

      if (!isCompatible) {
        String databaseDebugInfo =
            missingTables.length == BS.requiredTables.length
                ? '** FILE SEEMS NOT TO BE A NEWPIPE ZIP! **\n\n'
                : '';
        databaseDebugInfo += 'Missing Tables:\n$missingTables\n\n';

        /// Get list of tables in the database we're checking
        List<String?> actualTables = getDbTableList(dbPath);
        databaseDebugInfo += 'Actual Tables:\n$actualTables\n\n';

        /// And its schema...
        databaseDebugInfo += 'Schema:\n';
        for (final tableName in actualTables) {
          ResultSet tableResultSet = db.select('''
            SELECT sql FROM sqlite_schema
            WHERE name = '${tableName}';
          ''');
          databaseDebugInfo += '${tableResultSet.toString()}\n';
        }

        databaseDebugInfo += '\n-------\n\n';

        AppState.debug(databaseDebugInfo, alwaysOnScreen: true);
      }
      db.dispose();
    });

    return isCompatible;
  }

  /// Return a list of all table names from a database.
  static List<String?> getDbTableList(dbPath) {
    final List<String> tableNames = [];

    tryAndCatchErrors(() {
      final ResultSet resultSet = SqlHelpers.getSelectResultSet(
          dbPath, "SELECT name FROM sqlite_master WHERE type='table';");

      for (final row in resultSet) {
        tableNames.add(row['name']);
      }
    });

    return tableNames;
  }

  /// Run a SELECT query and return the results.
  static ResultSet getSelectResultSet(dbPath, sqlQuery) {
    ResultSet resultSet = ResultSet([], [], []);

    tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      resultSet = db.select(sqlQuery);
      db.dispose();
    });

    return resultSet;
  }

  /// Select/return specific columns from a single table.
  static ResultSet getFilteredColumns(dbPath, tableId, columns) {
    return getSelectResultSet(dbPath, 'SELECT $columns FROM $tableId;');
  }
}
