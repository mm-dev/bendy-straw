// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';
import 'preferences.dart';

/// Button for the main UI, leading to display of the [Preferences] widget.
class PreferencesButton extends StatefulWidget {
  const PreferencesButton({super.key});

  @override
  State<PreferencesButton> createState() => _PreferencesButtonState();
}

class _PreferencesButtonState extends State<PreferencesButton> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        Tooltip(
          message: 'Preferences',
          child: FloatingActionButton(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(BS.cornerButtonRadius),
              )),
              backgroundColor: Theme.of(context).colorScheme.surface,
              foregroundColor: Theme.of(context).colorScheme.onSurface,
              heroTag: null,
              elevation: BS.highButtonElevation,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Preferences()),
                );
              },
              child: const Icon(Icons.density_medium)),
        ),
      ])
    ]);
  }
}
