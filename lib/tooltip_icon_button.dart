// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';

enum TooltipIconButtonLayout {
  iconOnLeft,
  iconOnRight;
}

/// A button with an icon which displays a tooltip on hover.
class TooltipIconButton extends StatelessWidget {
  final void Function()? onPressedCallback;
  final String text;
  final Icon? icon;
  final double? elevation;
  final TooltipIconButtonLayout? layout;

  const TooltipIconButton({
    super.key,
    required this.onPressedCallback,
    required this.text,
    this.icon,
    this.elevation = 1,
    this.layout,
  });

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: text,
      child: ElevatedButton(
        onPressed: onPressedCallback,
        style: ElevatedButton.styleFrom(
          elevation: elevation,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (icon != null && layout == TooltipIconButtonLayout.iconOnLeft)
              ...(() {
                return [icon!, const SizedBox(width: BS.paddingSmall)];
              }()),
            Flexible(
              child: Container(
                  constraints: BoxConstraints.loose(
                      const Size.fromWidth(BS.buttonMaxWidth)),
                  child: Text(overflow: TextOverflow.ellipsis, text)),
            ),
            if (icon != null && layout == TooltipIconButtonLayout.iconOnRight)
              ...(() {
                return [const SizedBox(width: BS.paddingSmall), icon!];
              }())
          ],
        ),
      ),
    );
  }
}
