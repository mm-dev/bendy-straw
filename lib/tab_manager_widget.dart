// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'app_state.dart';
import 'constants.dart';
import 'database_viewer_widget.dart';
import 'db_manager.dart';
import 'tooltip_icon_button.dart';

/// Manage tabs.
///
/// Multiple database files can be opened at once. Each database is displayed
/// in a tab. The tabs are added/removed here.
class TabManagerWidget extends StatefulWidget {
  const TabManagerWidget({super.key});

  @override
  State<TabManagerWidget> createState() => _TabManagerWidgetState();
}

class _TabManagerWidgetState extends AppState<TabManagerWidget>
    with TickerProviderStateMixin {
  @override
  List<String>? listenForChanges = ['databases'];

  late TabController _tabController = TabController(length: 0, vsync: this);
  final PageStorageBucket _bucket = PageStorageBucket();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _updateTabController();

    Map databases = AppState.get('databases');

    return Column(
      children: [
        Container(
          /// This filled background covers the [BaseInfoWidget] which remains
          /// underneath the app content.
          color: _tabController.length > 0
              ? Theme.of(context).colorScheme.surface
              : Colors.transparent,

          /// This SizedBox is here to fix non-left-aligned tabs on Android
          child: Padding(
            padding: const EdgeInsets.only(
              right: BS.tabBarRightPadding,
            ),
            child: SizedBox(
              width: double.infinity,
              child: TabBar(
                controller: _tabController,
                isScrollable: true,
                tabAlignment: TabAlignment.start,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorPadding: const EdgeInsets.only(
                  left: BS.paddingLarge,
                  right: BS.paddingLarge * 2,
                ),
                automaticIndicatorColorAdjustment: true,

                ///
                /// Tabs
                ///
                tabs: databases.keys
                    .map<Widget>((dbPath) => Theme(
                        data: DbManager.getDbCurrentThemeData(dbPath, context),
                        child: Builder(builder: (context) {
                          return Container(
                              decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.surface,
                                borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(BS.cornerRadius),
                                  topLeft: Radius.circular(BS.cornerRadius),
                                ),
                              ),
                              padding: const EdgeInsets.only(
                                top: BS.paddingSmall,
                                bottom: BS.paddingSmall,
                                left: BS.paddingLarge,
                                right: BS.paddingSmall,
                              ),
                              child: Tab(
                                height: BS.tabHeight,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      DbManager.getPrettyName(dbPath),
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .primary),
                                    ),
                                    const SizedBox(width: BS.paddingMedium),
                                    IconButton(
                                        alignment: Alignment.center,
                                        visualDensity: VisualDensity.compact,
                                        padding: EdgeInsets.zero,
                                        onPressed: () {
                                          _confirmCloseTabAndDatabase(dbPath);
                                        },
                                        icon: const Icon(Icons.close_rounded))
                                  ],
                                ),
                              ));
                        })))
                    .toList(),
              ),
            ),
          ),
        ),
        Expanded(

            ///
            /// TabBarViews
            ///
            child: PageStorage(
          bucket: _bucket,
          child: TabBarView(
            controller: _tabController,
            children: databases.keys
                .map<Widget>((dbPath) => DatabaseViewerWidget(
                      key: PageStorageKey<String>('${dbPath}_TabBarView'),
                      dbPath: dbPath,
                    ))
                .toList(),
          ),
        ))
      ],
    );
  }

  /// Start the close tab/database process by displaying a dialog with a
  /// confirm button, which will close the tab/database when pressed.
  void _confirmCloseTabAndDatabase(String dbPath) {
    if (DbManager.getDbDirtyState(dbPath)) {
      AppState.showAppDialog(title: 'Discard unsaved changes?', message: '''
This database has unsaved changes, you'll lose them if you close it.

If you want to keep your changes, close this popup and press the 'Export' button.
        ''', actions: <Widget>[
        TooltipIconButton(
          text: 'Discard and close',
          icon: const Icon(Icons.thumb_down_rounded),
          layout: TooltipIconButtonLayout.iconOnRight,
          onPressedCallback: () {
            _closeTabAndDatabase(dbPath);
            Navigator.of(context).pop();
          },
        ),
      ]);
    } else {
      _closeTabAndDatabase(dbPath);
    }
  }

  /// Close the tab and associated database.
  void _closeTabAndDatabase(String dbPath) {
    if (DbManager.removeDbByPathKey(dbPath)) {
      AppState.update('databases', DbManager.databases, forceRebuild: true);

      AppState.get('localPlaylistSelectedRows').remove(dbPath);
      AppState.get('subscriptionsSelectedRows').remove(dbPath);
      AppState.get('remotePlaylistsSelectedRows').remove(dbPath);
    }
  }

  /// Select and display a specific database based on the [index] number of a
  /// tab.
  void _setCurrentSelectedDbPathByIndex(index) {
    String currentSelectedDbPath = '';
    if (DbManager.numberOfDatabases > 0) {
      currentSelectedDbPath = AppState.get('databases').keys.toList()[index];
    }

    AppState.update('currentSelectedDbPath', currentSelectedDbPath);
  }

  /// Update the tab controller to show the currently-opened databases and
  /// display the currently-selected database.
  void _updateTabController() {
    int totalDatabases = DbManager.numberOfDatabases;
    if (totalDatabases != _tabController.length) {
      int lastTabIndex = (totalDatabases > 0) ? totalDatabases - 1 : 0;

      _tabController.dispose();
      _tabController = TabController(
        length: totalDatabases,
        initialIndex: lastTabIndex,
        animationDuration: Duration.zero,
        vsync: this,
      );
      _tabController.addListener(() {
        if (!_tabController.indexIsChanging) {
          _setCurrentSelectedDbPathByIndex(_tabController.index);
        }
      });

      _setCurrentSelectedDbPathByIndex(lastTabIndex);
    }
  }
}
