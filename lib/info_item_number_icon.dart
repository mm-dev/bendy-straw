// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';

/// Display a number in an icon-type format.
class InfoItemNumberIcon extends StatelessWidget {
  const InfoItemNumberIcon({
    super.key,
    required this.itemNumberLabel,
  });

  final String itemNumberLabel;

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: ColoredBox(
        color: Theme.of(context).colorScheme.secondary,
        child: SizedBox(
          width: BS.numberIconSize,
          height: BS.numberIconSize,
          child: Align(
            alignment: Alignment.center,
            child: Text(
              itemNumberLabel,
              style: TextStyle(
                color: Theme.of(context).colorScheme.primaryContainer,
                fontWeight: FontWeight.bold,
                fontSize: BS.fontSizeNumberIcon,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
