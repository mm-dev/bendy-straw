// SPDX-License-Identifier: GPL-3.0-only

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:observable/observable.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Add this widget to the tree to enable shared state in the app.
///
/// All widgets which want to access shared state should be children of this
/// widget. Make this widget high enough in the tree that it wraps all of the
/// children which need it, but no higher (for efficiency).
class AppStateWidget extends StatelessWidget {
  final Widget child;

  /// Set some default empty variables.
  static final ObservableMap _appState = ObservableMap.from({
    'preferences': null,
    'debug': '',
    'totalDbsOpenedSinceSessionStart': 0,
    'dialogToShow': {},
    'databases': {},
    'localPlaylistSelectedRows': {},
    'remotePlaylistsSelectedRows': {},
    'subscriptionsSelectedRows': {},
    'currentSelectedDbPath': '',
    'aDatabaseWasDirtied': null
  });

  const AppStateWidget({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    /// Set up initial instance of [SharedPreferences], it's async so we need
    /// to use [FutureBuilder].
    return FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (context, AsyncSnapshot<SharedPreferences> snapshot) {
          if (snapshot.hasData) {
            AppState.update('preferences', snapshot.data);
            // Wipe stored preferences (useful for debugging).
            // snapshot.data?.clear();
          }
          return child;
        });
  }
}

/// Contain and manage shared app state.
///
/// Any widget that wants to access app state should extend this class in its
/// [createState], eg:
/// ``` dart
/// class _TabManagerWidgetState extends AppState<TabManagerWidget>
/// ```
/// When a change is made to any element in [listenForChanges], the widget will
/// be rebuilt.
abstract class AppState<T extends StatefulWidget> extends State<T> {
  StreamSubscription? _appStateChangeSubscription;

  /// List of [_appState] keys on which extending classes will listen for
  /// changes. When the value related to a listened key changes, this widget
  /// will rebuild, eg:
  ///
  /// ``` dart
  /// listenForChanges = ['mainData', 'userNames'];
  /// ```
  /// The above code means this widget will be rebuilt when
  /// [_appState['mainData'] or [_appState['userNames']] is changed.
  List<String>? get listenForChanges;
  set listenForChanges(List<String>? value);

  @override
  void initState() {
    super.initState();

    /// Listen for changes in [_appState] and rebuild any interested widgets
    /// (those which are listening for the specific key which changed).
    _appStateChangeSubscription =
        AppStateWidget._appState.changes.listen((List event) {
      for (ChangeRecord change in event) {
        if (change is MapChangeRecord) {
          if (listenForChanges != null &&
              listenForChanges!.contains(change.key)) {
            String widgetDescription = widget.key?.toString() ??
                '${widget.toStringShort()}::${widget.hashCode}';

            /// if the key was removed, that indicates this is just the first
            /// step in a forced rebuild, so ignore this change - another
            /// change of the same key will follow immediately and we don't
            /// want to rebuild twice.
            if (!change.isRemove) {
              print('$widgetDescription HEARD: \'${change.key}\'');
              setState(() {});
            }
          }
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _appStateChangeSubscription?.cancel();
  }

  /// Update a part (based on [key]) of the shared state of the app.
  static void update(String key, dynamic newValue, {forceRebuild = false}) {
    if (forceRebuild) {
      /// Sometimes [newValue] may be an already-existing object, where only
      /// the internals have changed. However, because the object is 'still the
      /// same object' (as it is only looked at in a shallow way),
      /// `AppStateWidget._appState.changes()` won't fire.
      ///
      /// In this case, `forceRebuild` can be passed as `true` to work around
      /// this non-detection of the internal object changes. If so, we remove
      /// the object, then add it again, forcing the change to be recognised.
      ///
      /// This causes the `changes()` event to fire twice, first when the object
      /// is removed, then again when the object is re-added. So
      /// `_appStateChangeSubscription` has to watch out for this and ignore
      /// the first change (otherwise the widget tree gets rebuilt twice).
      AppStateWidget._appState.remove(key);
      print('Force REBUILD for widgets listening to: \'${key}\'');
    }
    AppStateWidget._appState[key] = newValue;
  }

  /// Get a part (based on [key]) of the shared state of the app.
  static dynamic get(String key) {
    return AppStateWidget._appState[key];
  }

  /// Output debugging info.
  ///
  /// Print the info to the console, and if `alwaysOnScreen` is `true`, also
  /// display the info in a dialog.
  static void debug(str, {alwaysOnScreen = false}) {
    print(str);
    if (alwaysOnScreen) {
      AppState.update('debug', str.toString());
      //AppState.update('debug', '${AppState.get('debug')}\n${str.toString()}');
      showAppDialog(
          title: 'Error', message: AppState.get('debug'), isError: true);
    }
  }

  /// Pass debugging info to the app's main dialog for display.
  static void showAppDialog(
      {required String title,
      String? message,
      Widget? content,
      List<Widget>? actions,
      bool isError = false}) {
    AppState.update(
        'dialogToShow',
        {
          'title': title,
          'content': content,
          'message': message,
          'isError': isError,
          'actions': actions
        },
        forceRebuild: true);
  }
}
