// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';
import 'dialog_dismiss_button.dart';
import 'enum.dart';

/// Header for the [DestinationChooser] widget, with a title and a 'dismiss'
/// button for the dialog.
class DestinationChooserHeader extends StatelessWidget {
  const DestinationChooserHeader({
    super.key,
    required this.validForCopyDbPaths,
    required this.isMoveMode,
    required this.tableType,
    this.itemCount,
  });

  final List<String> validForCopyDbPaths;
  final TableType tableType;
  final bool isMoveMode;
  final int? itemCount;

  @override
  Widget build(BuildContext context) {
    String itemType = '';
    if (tableType == TableType.localPlaylist) {
      if (itemCount == 0) {
        itemType = 'playlist';
      } else {
        itemType = 'stream';
      }
    } else if (tableType == TableType.subscriptions) {
      itemType = 'channel';
    } else if (tableType == TableType.remotePlaylists) {
      itemType = 'playlist';
    }
    String itemDescription = itemCount == null
        ? 'playlist'
        : '${itemCount} ${itemCount == 1 ? itemType : '${itemType}s'}';
    TextStyle style = const TextStyle(fontSize: BS.fontSizeHeading);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
                top: BS.paddingMedium, bottom: BS.paddingMedium),
            child: validForCopyDbPaths.isEmpty
                ? Text('Nowhere to copy to!', style: style)
                : Text(
                    isMoveMode
                        ? 'Move $itemDescription to?'
                        : 'Copy $itemDescription to?',
                    style: style),
          ),
        ),
        const DialogDismissButton()
      ],
    );
  }
}
