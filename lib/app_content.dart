// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_state.dart';
import 'base_info_widget.dart';
import 'constants.dart';
import 'dialog_dismiss_button.dart';
import 'file_manager_widget.dart';
import 'preferences_button.dart';
import 'screen.dart';
import 'tab_manager_widget.dart';
import 'tooltip_icon_button.dart';

// Outer wrapper for the entire app.
class AppContent extends StatefulWidget {
  const AppContent({super.key});

  @override
  State<AppContent> createState() => _AppContentState();
}

class _AppContentState extends AppState<AppContent> {
  @override
  List<String>? listenForChanges = ['dialogToShow'];

  @override
  Widget build(BuildContext context) {
    Map dialogToShow = AppState.get('dialogToShow');
    if (dialogToShow.isNotEmpty) {
      Future.delayed(Duration.zero, () {
        if (context.mounted) {
          _showDialog(context, dialogToShow);
        }
      });
    }

    return LayoutBuilder(builder: (context, constraints) {
      return const Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              /// Background instructions
              Positioned.fill(child: BaseInfoWidget()),
          
              /// Main app on top
              Positioned.fill(
                child: AppStateWidget(
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                  Expanded(
                    child: TabManagerWidget(),
                  ),
                ])),
              ),
          
              /// Floating buttons
              Positioned.fill(child: FileManagerWidget()),
              Positioned.fill(child: PreferencesButton()),
            ],
          ),
        ),
      );
    });
  }

  /// Reusable dialog to be used by other classes as much as possible.
  void _showDialog(context, dialogToShow) {
    final Map dialogLayout = Screen.isCompact(context)
        ? BS.dialogLayoutCompact
        : BS.dialogLayoutStandard;

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              titlePadding: EdgeInsets.only(
                top: dialogLayout['topPad'],
                right: dialogLayout['rightPad'],
                bottom: dialogLayout['bottomPad'],
                left: dialogLayout['leftPad'],
              ),
              contentPadding: EdgeInsets.only(
                top: dialogLayout['topPad'],
                right: dialogLayout['rightPad'],
                bottom: dialogLayout['bottomPad'],
                left: dialogLayout['leftPad'],
              ),
              actionsPadding: EdgeInsets.only(
                top: dialogLayout['topPad'],
                right: dialogLayout['rightPad'],
                bottom: dialogLayout['bottomPad'],
                left: dialogLayout['leftPad'],
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(top: dialogLayout['topPad']),
                      child: Text(dialogToShow['title']),
                    ),
                  ),
                  const DialogDismissButton()
                ],
              ),
              content: dialogToShow['content'] ??
                  Container(
                      decoration: dialogToShow['isError']
                          ? BoxDecoration(
                              color: Theme.of(context).colorScheme.error,
                              borderRadius:
                                  BorderRadius.circular(BS.cornerRadius),
                            )
                          : null,
                      child: dialogToShow['message'] == null
                          ? const Text('')
                          : SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: dialogToShow['isError']
                                  ? Padding(
                                      padding: EdgeInsets.only(
                                        top: dialogLayout['innerTopPad'],
                                        right: dialogLayout['innerRightPad'],
                                        bottom: dialogLayout['innerBottomPad'],
                                        left: dialogLayout['innerLeftPad'],
                                      ),
                                      child: Text(dialogToShow['message'],
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .onError)),
                                    )
                                  : Text(
                                      dialogToShow['message'],
                                    ))),
              actions: <Widget>[
                Align(
                  alignment: Alignment.centerRight,
                  child:
                      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    if (dialogToShow['isError'])
                      TooltipIconButton(
                        text: 'Copy error to clipboard',
                        icon: const Icon(Icons.copy_rounded),
                        layout: TooltipIconButtonLayout.iconOnLeft,
                        onPressedCallback: () async {
                          await Clipboard.setData(
                              ClipboardData(text: dialogToShow['message']));
                        },
                      ),
                    if (dialogToShow['actions'] != null)
                      ...dialogToShow['actions']
                  ]),
                )
              ]);
        });

    /// Important to wipe [dialogToShow] otherwise the dialog gets redrawn
    /// every time build occurs for this widget (eg when screen size or
    /// orientation changes), resulting in multiple instances remaining
    /// on-screen.
    AppState.update('dialogToShow', {});
  }
}
