// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';

/// Helper class for getting display size.
class Screen {
  static Size size(BuildContext context) => MediaQuery.of(context).size;
  static double width(BuildContext context) => size(context).width;
  static double height(BuildContext context) => size(context).height;

  /// Return whether this is a small/compact display or not.
  static bool isCompact(BuildContext context) {
    return MediaQuery.of(context).size.width < BS.breakpointCompact;
  }
}
