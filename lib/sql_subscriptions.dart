// SPDX-License-Identifier: GPL-3.0-only

import 'package:sqlite3/sqlite3.dart';

import 'constants.dart';
import 'sql_string_maker.dart';
import 'sql_helpers.dart';

/// Low level SQL functions to operate on the list of channel subscriptions.
///
/// Only [DbManager] should call functions directly from this class. Other
/// classes should call  wrapper functions in [DbManager].
class SqlChannelSubscriptions {
  static void deleteItemsFromChannelSubscriptions(dbPath, subscriptionUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      db.execute('''
          DELETE FROM ${BS.subscriptionsTableId}
          WHERE uid IN ( ${subscriptionUids.join(", ")} )
        ''');
      db.dispose();
    });
  }

  /// Copy channel subscriptions from one database to another.
  static void copyItemsFromChannelSubscriptions(
      dbPathFrom, dbPathTo, subscriptionUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database dbFrom = sqlite3.open(dbPathFrom);

      /// Attach both databases with aliases so we can be clear which we're
      /// dealing with. Attaching dbFrom to itself seems strange but the alias
      /// is useful for clarity.
      dbFrom.execute('''
        ATTACH DATABASE '$dbPathTo' AS 'dbTo';
        ATTACH DATABASE '$dbPathFrom' AS 'dbFrom';
      ''');

      for (final subscriptionUid in subscriptionUids) {
        dbFrom.execute('''
            INSERT INTO dbTo.${BS.subscriptionsTableId} 
              ( ${SqlStringMaker.subscriptionsColumnsWithQuotesAndCommas} )
            SELECT ${SqlStringMaker.subscriptionsColumnsWithCommas}
            FROM dbFrom.${BS.subscriptionsTableId}
            WHERE uid = '$subscriptionUid'
            ON CONFLICT DO UPDATE SET url=url
            ;
          ''');
      }

      dbFrom.dispose();
    });
  }
}
