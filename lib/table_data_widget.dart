// SPDX-License-Identifier: GPL-3.0-only

import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:sqlite3/sqlite3.dart';

import 'app_state.dart';
import 'constants.dart';
import 'db_manager.dart';
import 'enum.dart';
import 'screen.dart';

/// Display a table.
///
/// Can be one of several types:
/// - [TableType.localPlaylist]: Local playlist
/// - [TableType.remotePlaylists]: List of remote playlists
/// - [TableType.subscriptions]: List of channel subscriptions
class TableDataWidget extends StatefulWidget {
  final String tableId;
  final TableType tableType;
  final ResultSet resultSet;
  final String dbPath;
  final Map<String, bool> selectedStateMap = {};
  late final int primaryKeyColumnIndex;
  late final double smRatio;
  late final double lmRatio;

  TableDataWidget({
    super.key,
    required this.tableId,
    required this.resultSet,
    required this.dbPath,
    required this.tableType,
  }) {
    primaryKeyColumnIndex = _getPrimaryKeyColumnIndex();

    /// Build list of each UID and whether it is selected or not
    for (List rowCells in resultSet.rows) {
      String uid = rowCells[primaryKeyColumnIndex].toString();
      selectedStateMap[uid] = false;
    }

    /// [DataColumn2] takes floating point ratios [smRatio]/[lmRatio] to calculate
    /// small, medium and large  column sizes.
    /// Calculate them here using the more intuitive flex concept.
    double smallColumnFlex = 2.5;
    double mediumColumnFlex = 4;
    double largeColumnFlex = 6;
    smRatio = smallColumnFlex / mediumColumnFlex;
    lmRatio = largeColumnFlex / mediumColumnFlex;
  }

  @override
  State<TableDataWidget> createState() => _TableDataWidgetState();

  /// Get the index of the column containing the primary key for a table.
  ///
  /// Column info is described in enums, whose values have a boolean
  /// [isPrimaryKey] field. Here we find the primary key for the current table
  /// based on which kind of table it is (eg custom playlist, remote playlist,
  /// channel subscriptions).
  int _getPrimaryKeyColumnIndex() {
    int primaryKeyIndex = -1;
    dynamic columnConfig;
    int columnIndex = 0;
    do {
      try {
        columnConfig = _getColumnConfig(columnIndex: columnIndex);
        if (columnConfig.isPrimaryKey != null && columnConfig.isPrimaryKey) {
          primaryKeyIndex = columnIndex;
          break;
        }
        columnIndex++;
      } on Exception catch (e) {
        AppState.debug('EXCEPTION:\n$e', alwaysOnScreen: true);
        break;
      } catch (e) {
        AppState.debug('ERROR:\n$e', alwaysOnScreen: true);
        break;
      }
    } while (columnConfig != null);

    return primaryKeyIndex;
  }

  /// Get data describing how to work witth a specifc column.
  dynamic _getColumnConfig({required int columnIndex}) {
    return switch (tableType) {
      TableType.localPlaylist => LocalPlaylistColumns.values[columnIndex],
      TableType.remotePlaylists => RemotePlaylistsColumns.values[columnIndex],
      TableType.subscriptions => SubscriptionsColumns.values[columnIndex],
    };
  }
}

class _TableDataWidgetState extends AppState<TableDataWidget> {
  late Color rowColorSelected;
  late Color rowColorEven;

  int? currentSortedColumnIndex;
  bool currentSortedAscending = false;

  @override
  late List<String>? listenForChanges = switch (widget.tableType) {
    TableType.localPlaylist => ['localPlaylistSelectedRows'],
    TableType.remotePlaylists => ['remotePlaylistsSelectedRows'],
    TableType.subscriptions => ['subscriptionsSelectedRows'],
  };

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    rowColorSelected = Theme.of(context).colorScheme.primary.withOpacity(0.3);
    rowColorEven =
        Theme.of(context).colorScheme.surfaceContainerHighest.withOpacity(0.3);
  }

  @override
  void initState() {
    super.initState();
    // Recreate row selection from [AppState]
    List? preselectedFromAppState = switch (widget.tableType) {
      TableType.localPlaylist =>
        AppState.get('localPlaylistSelectedRows')?[widget.dbPath]
            ?[widget.tableId],
      TableType.remotePlaylists =>
        AppState.get('remotePlaylistsSelectedRows')?[widget.dbPath]
            ?[widget.tableId],
      TableType.subscriptions =>
        AppState.get('subscriptionsSelectedRows')?[widget.dbPath]
            ?[widget.tableId],
    };
    if (preselectedFromAppState != null) {
      widget.selectedStateMap.forEach((rowUid, isSelected) {
        widget.selectedStateMap[rowUid] =
            preselectedFromAppState.contains(rowUid);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    bool primaryKeyColumnIsHidden =
        AppState.get('preferences')?.getBool('primaryKeyColumnIsHidden') ??
            BS.defaultPreferences['primaryKeyColumnIsHidden'];
    final CheckboxThemeData checkboxThemeData = CheckboxThemeData(
        fillColor: WidgetStateProperty.resolveWith((states) {
          return states.contains(WidgetState.selected)
              ? Theme.of(context).colorScheme.primary
              : null;
        }),
        checkColor: WidgetStateProperty.resolveWith((states) {
          return states.contains(WidgetState.selected)
              ? Theme.of(context).colorScheme.onPrimary
              : null;
        }),
        side: BorderSide(
            color: Theme.of(context).colorScheme.surfaceContainerHighest,
            width: BS.checkboxBorderWidth));

    Map tableLayout = Screen.isCompact(context)
        ? BS.tableLayoutCompact
        : BS.tableLayoutStandard;

    return Container(
      constraints: const BoxConstraints(maxHeight: BS.tableWidgetHeight),
      child: DataTable2(
          headingTextStyle: tableLayout['headingTextStyle']
              .copyWith(color: Theme.of(context).colorScheme.secondary),
          headingRowHeight: tableLayout['headingRowHeight'],
          headingCheckboxTheme: checkboxThemeData,
          checkboxHorizontalMargin: tableLayout['checkboxHorizontalMargin'],
          datarowCheckboxTheme: checkboxThemeData,
          dividerThickness: 0,
          sortColumnIndex: currentSortedColumnIndex,
          sortAscending: currentSortedAscending,
          smRatio: widget.smRatio,
          lmRatio: widget.lmRatio,
          columnSpacing: tableLayout['columnSpacing'],
          horizontalMargin: tableLayout['horizontalMargin'],
          columns: widget.resultSet.columnNames
              .asMap()
              .entries

              /// WRAPPED_COLUMN_ENTRY
              /// To facilitate hiding the primary key column in the UI:
              ///
              /// Wrap the [entry.value] (which at this point in the chain is
              /// the column name) in a [Map], also adding the column config
              /// info.
              ///
              /// We need the column config to find out if it [isPrimaryKey],
              /// in which case we can filter it out using [where]. This all
              /// has to be done before we get to returning the [DataColumn2]
              /// as we can't return null for omitted columns.
              .map((entry) => MapEntry(entry.key, {
                    'columnName': entry.value,
                    'columnConfig':
                        widget._getColumnConfig(columnIndex: entry.key)
                  }))
              .where((entry) => !(primaryKeyColumnIsHidden &&
                  entry.value['columnConfig'].isPrimaryKey))
              .map((entry) {
            var columnConfig = entry.value['columnConfig'];
            return DataColumn2(
              label: Align(
                  alignment: columnConfig.numeric
                      ? Alignment.centerRight
                      : Alignment.center,
                  child: Text(entry.value['columnName'])),
              size: columnConfig.displaySize,
              numeric: columnConfig.numeric,
              onSort: columnConfig.canSortOn
                  ? _sortPlaylistTableByColumnIndex
                  : null,
            );
          }).toList(),
          // Convoluted way of getting indexes for map items
          rows: widget.resultSet.rows.asMap().keys.toList().map((index) {
            List row = widget.resultSet.rows[index];
            String rowUid = row[widget.primaryKeyColumnIndex].toString();
            return DataRow2(
                selected: widget.selectedStateMap[rowUid] ?? false,
                onSelectChanged: (value) {
                  widget.selectedStateMap[rowUid] = value!;
                  _updateSelectedState();
                },
                // Colour odd/even rows differently, override if row is selected.
                color: WidgetStateProperty.resolveWith<Color?>(
                    (Set<WidgetState> states) {
                  if (states.contains(WidgetState.selected)) {
                    return rowColorSelected;
                  } else {
                    return index.isEven ? rowColorEven : null;
                  }
                }),
                cells: row
                    .asMap()
                    .entries

                    /// See WRAPPED_COLUMN_ENTRY comment above
                    .map((entry) => MapEntry(entry.key, {
                          'columnName': entry.value,
                          'columnConfig':
                              widget._getColumnConfig(columnIndex: entry.key)
                        }))
                    .where((entry) => !(primaryKeyColumnIsHidden &&
                        entry.value['columnConfig'].isPrimaryKey))
                    .map((entry) {
                  var columnConfig = entry.value['columnConfig'];
                  return DataCell(
                    /// The enum describing the columns also contains a method
                    /// to return an appropriate wiget for its data type.
                    columnConfig.getContentWidget(entry.value['columnName']),
                  );
                }).toList());
          }).toList()),
    );
  }

  /// Update which items are selected.
  ///
  /// TODO Check that this is still correct in terms of the functionality, are there situations where we copy items from several playlists into one playlist? Or is it always a 1-to-1 copy?
  void _updateSelectedState() {
    // Get shared state
    Map selectedInEntireApp = switch (widget.tableType) {
      TableType.localPlaylist => AppState.get('localPlaylistSelectedRows'),
      TableType.remotePlaylists => AppState.get('remotePlaylistsSelectedRows'),
      TableType.subscriptions => AppState.get('subscriptionsSelectedRows'),
    };

    // Create key for this table's db if it doesn't already exist
    if (!selectedInEntireApp.containsKey(widget.dbPath)) {
      selectedInEntireApp[widget.dbPath] = {};
    }

    // Update this table's selections list
    List<String>? selectedInThisTable = [];
    widget.selectedStateMap.forEach((key, value) {
      if (value == true) {
        selectedInThisTable.add(key);
      }
    });
    if (selectedInThisTable.isNotEmpty) {
      selectedInEntireApp[widget.dbPath][widget.tableId] = selectedInThisTable;
    } else {
      print('Nothing selected in this table, removing key');
      selectedInEntireApp[widget.dbPath].remove(widget.tableId);
      if (selectedInEntireApp[widget.dbPath].isEmpty) {
        print('Nothing selected in this database, removing key');
        selectedInEntireApp.remove(widget.dbPath);
      }
    }

    // Update shared state
    switch (widget.tableType) {
      case TableType.localPlaylist:
        AppState.update('localPlaylistSelectedRows', selectedInEntireApp,
            forceRebuild: true);
      case TableType.remotePlaylists:
        AppState.update('remotePlaylistsSelectedRows', selectedInEntireApp,
            forceRebuild: true);
      case TableType.subscriptions:
        AppState.update('subscriptionsSelectedRows', selectedInEntireApp,
            forceRebuild: true);
    }
    print('selected rows: ${selectedInEntireApp}');
  }

  /// Sort a playlist based on the contents of a column.
  ///
  /// Using the contents of the column at [columnIndex], sort the playlist in
  /// ascending order if [ascending] is `true`, otherwise descending order.
  void _sortPlaylistTableByColumnIndex(int columnIndex, bool ascending) {
    dynamic columnConfig = widget._getColumnConfig(columnIndex: columnIndex);
    DbManager.sortLocalPlaylistByColumn(
        dbPath: widget.dbPath,
        playlistUid: widget.tableId,
        sortColumnIndex: columnIndex,
        isNumeric: columnConfig.numeric,
        streamUidColumnIndex: widget._getPrimaryKeyColumnIndex(),
        ascending: ascending);
    setState(() {
      currentSortedColumnIndex = columnIndex;
      currentSortedAscending = ascending;
    });
  }
}
