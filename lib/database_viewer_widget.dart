// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:sqlite3/sqlite3.dart' as sqlite3;

import 'app_state.dart';
import 'constants.dart';
import 'db_manager.dart';
import 'enum.dart';
import 'screen.dart';
import 'table_widget.dart';

/// Display a database.
///
/// This widget fills a tab with lots of [TableWidget]s, one for each table in
/// the database.
class DatabaseViewerWidget extends StatefulWidget {
  final String dbPath;

  const DatabaseViewerWidget({
    super.key,
    required this.dbPath,
  });

  @override
  State<DatabaseViewerWidget> createState() => _DatabaseViewerWidgetState();
}

class _DatabaseViewerWidgetState extends AppState<DatabaseViewerWidget> {
  /// Listen for changes on [dbPath], meaning we rebuild when something changes
  /// in this specific database (eg a table is deleted).
  @override
  late List<String>? listenForChanges = [widget.dbPath];

  @override
  Widget build(BuildContext context) {
    double sidePadding =
        Screen.isCompact(context) ? BS.paddingSmall : BS.paddingMedium;

    return Theme(
        data: DbManager.getDbCurrentThemeData(widget.dbPath, context),
        child: Builder(builder: (context) {
          return Container(
              color: Theme.of(context).colorScheme.surface,
              child: Column(

                  /// Main outer column containing all table widgets.
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: BS.paddingExtraLarge,
                          bottom: BS.paddingMedium,
                          left: sidePadding,
                          right: sidePadding,
                        ),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ///
                              /// Main tables ie Bookmarked Playlists and Channel Subs
                              ///
                              ...BS.knownTables.keys.map<Widget>((tableId) {
                                /// [_getTableDataFromId] returns a 2-part record
                                var (
                                  String displayName,
                                  sqlite3.ResultSet playlistResultSet,
                                  TableType? tableType
                                ) = _getTableDataFromId(tableId);
                                return tableType == null
                                    ? const Text('Unidentified tableType')
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: BS.paddingLarge),
                                        child: TableWidget(
                                            key: ValueKey(
                                                '${DbManager.getPrettyName(widget.dbPath)}::$displayName'),
                                            tableId: tableId,
                                            dbPath: widget.dbPath,
                                            displayName: displayName,
                                            resultSet: playlistResultSet,
                                            tableType: tableType),
                                      );
                              }),

                              ///
                              /// Custom playlists
                              ///
                              const Padding(
                                padding: EdgeInsets.only(
                                  right: BS.paddingMedium,
                                  left: BS.paddingMedium,
                                  bottom: BS.paddingMedium,
                                  top: BS.paddingLarge,
                                ),
                                child: Text(
                                  'Custom Playlists',
                                  style:
                                      TextStyle(fontSize: BS.fontSizeHeading),
                                ),
                              ),
                              ...DbManager.getLocalPlaylists(widget.dbPath)
                                  .entries
                                  .map<Widget>((e) {
                                String tableId = e.key;
                                Map metaData = e.value;
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: BS.paddingMedium),
                                  child: TableWidget(
                                      key: ValueKey(
                                          '${DbManager.getPrettyName(widget.dbPath)}::${metaData['displayName']}'),
                                      tableId: tableId,
                                      dbPath: widget.dbPath,
                                      displayName: metaData['displayName'],
                                      resultSet: metaData['rows'],
                                      tableType: TableType.localPlaylist),
                                );
                              }),
                              const SizedBox(height: BS.paddingExtraLarge)
                            ]),
                      ),
                    ))
                  ]));
        }));
  }

  /// Return data for a specific table.
  ///
  /// [tableId] must match a table from the database.
  (String, sqlite3.ResultSet, TableType?) _getTableDataFromId(String tableId) {
    String displayName = tableId;
    String tableColumns = '*';
    TableType? tableType;

    /// If this is a 'known table' ie not a local user-defined playlist, fill
    /// in some info from the constants file.
    Map? tableInfoEntry = BS.knownTables[tableId];
    if (tableInfoEntry != null) {
      displayName = tableInfoEntry['displayName'];
      tableType = tableInfoEntry['tableType'];
      tableColumns = _getDisplayColumnsString(tableId);
    }

    final sqlite3.ResultSet resultSet =
        DbManager.getFilteredColumns(widget.dbPath, tableId, tableColumns);

    return (displayName, resultSet, tableType);
  }

  /// For known tables, create strings for SQL queries describing their
  /// columns.
  String _getDisplayColumnsString(tableId) {
    late String columnList;
    switch (tableId) {
      case 'remote_playlists':
        columnList = RemotePlaylistsColumns.values
            .where((e) => e.isVisible)
            .map((e) => '${e.name} AS ${e.displayName}')
            .toList()
            .join(', ');
      case 'subscriptions':
        columnList = SubscriptionsColumns.values
            .where((e) => e.isVisible)
            .map((e) => '${e.name} AS ${e.displayName}')
            .toList()
            .join(', ');
    }
    return columnList;
  }
}
