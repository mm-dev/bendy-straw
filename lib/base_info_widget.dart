// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';
import 'info_row.dart';
import 'screen.dart';

/// Display app info/instructions.
///
/// This widget always sits at the bottom of the stack (see [AppContent]). It
/// gets covered by tabs and their content whenever databases are loaded, and
/// automatically revealed again when there is nothing loaded.
class BaseInfoWidget extends StatelessWidget {
  const BaseInfoWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final double vw = Screen.width(context) * .01;
    final double vh = Screen.height(context) * .01;
    bool screenIsCompact = Screen.isCompact(context);

    final int padPercent = screenIsCompact ? 2 : 5;

    final TextStyle textStyle = TextStyle(
      color: Theme.of(context).colorScheme.onPrimaryContainer,
      fontSize: BS.fontSizeBaseInfo,
    );

    double padding = screenIsCompact ? BS.paddingMedium : BS.paddingLarge;

    return Center(
      child: Padding(
          padding: EdgeInsets.only(
              top: vh * padPercent,
              left: vw * padPercent,
              right: vw * padPercent,
              bottom: vh * padPercent),
          child: SizedBox.expand(
            child: Container(
              padding: EdgeInsets.only(
              top:padding,
              right:padding,
              bottom:padding,
              left:BS.paddingMedium,
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius:
                    const BorderRadius.all(Radius.circular(BS.cornerRadius)),
              ),
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  InfoRow(
                    itemNumberLabel: '1',
                    mainInfo: [
                      TextSpan(text: 'Tap ', style: textStyle),
                      const WidgetSpan(
                          child: Icon(Icons.add_to_photos_rounded)),
                      TextSpan(
                          text: ' below and add a NewPipe zip',
                          style: textStyle),
                    ],
                    extraInfoList: const [
                      'In NewPipe:\nSettings > Content > Export database',
                      'You can open several files at the same time and copy/move items between them',
                    ],
                  ),
                  InfoRow(itemNumberLabel: '2', mainInfo: [
                    const WidgetSpan(child: SizedBox(height: BS.paddingMediumLarge)),
                    TextSpan(
                        text: 'Use BendyStraw to edit your data!',
                        style: textStyle)
                  ]),
                  InfoRow(
                    itemNumberLabel: '3',
                    mainInfo: [
                      TextSpan(text: 'Tap ', style: textStyle),
                      const WidgetSpan(
                          child: Icon(Icons.file_download_rounded)),
                      TextSpan(
                          text:
                              ' below to save changes, then import them into NewPipe',
                          style: textStyle),
                    ],
                    extraInfoList: const [
                      'In NewPipe:\nSettings > Content > Import database',
                      '...and choose your new file',
                    ],
                  ),
                ],
              )),
            ),
          )),
    );
  }
}
