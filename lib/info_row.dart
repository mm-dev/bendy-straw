// SPDX-License-Identifier: GPL-3.0-only

// ignore_for_file: unnecessary_to_list_in_spreads

import 'package:flutter/material.dart';

import 'constants.dart';
import 'info_item_number_icon.dart';

/// An information row, containing text information and an optional list of
/// added information.
class InfoRow extends StatelessWidget {
  const InfoRow({
    super.key,
    required this.mainInfo,
    this.extraInfoList,
    this.itemNumberLabel = '',
  });

  final List<InlineSpan>? mainInfo;
  final List<String>? extraInfoList;
  final String itemNumberLabel;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyleDim = TextStyle(
      color: Theme.of(context).colorScheme.secondary,
      fontSize: BS.fontSizeSmall,
      height: BS.infoRowLineHeight,
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: BS.paddingSmall,
            right: BS.paddingMedium,
          ),
          child: itemNumberLabel.isNotEmpty
              ? InfoItemNumberIcon(itemNumberLabel: itemNumberLabel)
              : null,
        ),
        Expanded(child: RichText(text: TextSpan(children: mainInfo))),
        if (extraInfoList != null)
          Expanded(
            child: RichText(
                text: TextSpan(
              children: [
                ...extraInfoList!
                    .map((e) => TextSpan(children: [
                          TextSpan(text: e, style: textStyleDim),
                          const TextSpan(
                              text: '\n\n',
                              style:
                                  TextStyle(height: BS.infoRowGapLineHeight)),
                        ]))
                    .toList()
              ],
            )),
          ),
      ],
    );
  }
}
