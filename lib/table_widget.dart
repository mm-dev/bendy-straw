// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'app_state.dart';
import 'package:sqlite3/sqlite3.dart' as sqlite3;

import 'constants.dart';
import 'custom_expansion_tile.dart';
import 'enum.dart';
import 'playlist_header_widget.dart';
import 'screen.dart';
import 'selected_items_buttons_widget.dart';
import 'table_data_widget.dart';
import 'playlist_icon_and_item_count.dart';

/// A widget for displaying and managing a single database table.
///
/// Put together the varius parts needed to display a table from the database:
/// - [PlaylistHeaderWidget]: The editable name of the playlist and buttons to perform actions on the playlist as a whole
/// - [TableDataWidget]: The data contained within the table
/// - [SelectedItemsButtonsWidget]: When items (eg streams) are selected in a table, these buttons perform action (eg copy/move) on those items
class TableWidget extends StatelessWidget {
  final String displayName;
  final String tableId;
  final String dbPath;
  final sqlite3.ResultSet resultSet;
  final TableType tableType;

  const TableWidget({
    super.key,
    required this.displayName,
    required this.tableId,
    required this.dbPath,
    required this.resultSet,
    required this.tableType,
  });

  @override
  Widget build(BuildContext context) {
    late String pageStorageKeyPrefix = '${dbPath}::${tableId}';

    late Color dataTableBackgroundColor =
        Theme.of(context).colorScheme.surface;
    late Color wrapperBackgroundColor = Theme.of(context).colorScheme.surface;

    bool screenIsCompact = Screen.isCompact(context);
    double sidePadding = screenIsCompact ? BS.paddingSmall : BS.paddingMedium;

    bool tableUidIsHidden =
        AppState.get('preferences')?.getBool('tableUidIsHidden') ??
            BS.defaultPreferences['tableUidIsHidden'];

    return Card(
      key: ObjectKey('${pageStorageKeyPrefix}_Card'),
      color: wrapperBackgroundColor,
      child: CustomExpansionTile(
        tilePadding: EdgeInsets.only(
          //left: sidePadding,
          right: sidePadding,
        ),

        /// Unique key to avoid redraw errors with values not changing, loss of
        /// scroll position, collapsed state etc.
        key: PageStorageKey<String>(
            '${pageStorageKeyPrefix}_CustomExpansionTile'),

        ///
        ///
        /// Icon and item count.
        ///
        /// In a compact layout we want this to display at the end of the
        /// expansion header (trailing), otherwise at the start (leading). Two
        /// ternary conditions are used to achieve this, it's not very elegant
        /// but it works and is fairly concise.
        ///
        leading: screenIsCompact
            ? null
            : PlaylistIconAndItemCount(
                tableType: tableType, resultSet: resultSet),
        trailing: screenIsCompact
            ? PlaylistIconAndItemCount(
                tableType: tableType, resultSet: resultSet)
            : null,

        ///
        ///
        /// Rest of the header plus the main content.
        ///
        title: tableType == TableType.localPlaylist
            ? PlaylistHeaderWidget(
                key: PageStorageKey<String>(
                    '${pageStorageKeyPrefix}_PlaylistHeaderWidget'),
                name: displayName,
                dbPath: dbPath,
                tableId: tableId,
              )
            : Padding(
                padding: const EdgeInsets.only(left: BS.paddingMedium),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(displayName,
                        style: const TextStyle(fontWeight: FontWeight.bold))),
              ),
        children: [
          Container(
            color: dataTableBackgroundColor,
            child: TableDataWidget(
                key: PageStorageKey<String>(
                    '${pageStorageKeyPrefix}_TableDataWidget'),
                tableType: tableType,
                tableId: tableId,
                resultSet: resultSet,
                dbPath: dbPath),
          ),
          Padding(
            padding: const EdgeInsets.all(BS.paddingMedium),
            child: Row(
              children: [
                SelectedItemsButtonsWidget(
                  tableType: tableType,
                  tableId: tableId,
                  tableDbPath: dbPath,
                ),
                if (!tableUidIsHidden)
                  Expanded(
                    child: Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          'Playlist UID: $tableId',
                          style: TextStyle(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onSurface
                                  .withOpacity(BS.disableControlOpacity)),
                        )),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
