// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_state.dart';
import 'constants.dart';
import 'screen.dart';

/// A table cell specialised for showing a URL.
class TableCellUrl extends StatelessWidget {
  final dynamic value;

  const TableCellUrl({
    super.key,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    late final Uri uri = Uri.parse(value);
    Map cellLayout = Screen.isCompact(context)
        ? BS.tableCellLayoutCompact
        : BS.tableCellLayoutStandard;
    bool shortenUrlsForDisplay =
        AppState.get('preferences')?.getBool('shortenUrlsForDisplay') ?? true;
    return Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: cellLayout['padding'],
          child: Tooltip(
            message: 'Open $value',
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                visualDensity: VisualDensity.compact,
                padding: cellLayout['urlButtonPadding'],
              ),
              onPressed: () async {
                if (!await launchUrl(uri)) {
                  throw Exception(value);
                }
              },
              child: Text(
                '${shortenUrlsForDisplay ? '' : uri.authority}${uri.path}/${uri.query}',
                style: cellLayout['textStyle'],
              ),
            ),
          ),
        ));
  }
}
