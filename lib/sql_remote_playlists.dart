// SPDX-License-Identifier: GPL-3.0-only

import 'package:sqlite3/sqlite3.dart';

import 'constants.dart';
import 'sql_string_maker.dart';
import 'sql_helpers.dart';

/// Low level SQL functions to operate on the list of remote playlists.
///
/// Only [DbManager] should call functions directly from this class. Other
/// classes should call  wrapper functions in [DbManager].
class SqlRemotePlaylists {
  static void deleteItemsFromRemotePlaylists(dbPath, playlistUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      db.execute('''
          DELETE FROM ${BS.remotePlaylistsTableId}
          WHERE uid IN ( ${playlistUids.join(", ")} )
        ''');
      db.dispose();
    });
  }

  /// Copy remote playlists from one database to another.
  static void copyItemsFromRemotePlaylists(dbPathFrom, dbPathTo, playlistUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database dbFrom = sqlite3.open(dbPathFrom);

      /// Attach both databases with aliases so we can be clear which we're
      /// dealing with. Attaching dbFrom to itself seems strange but the alias
      /// is useful for clarity.
      dbFrom.execute('''
        ATTACH DATABASE '$dbPathTo' AS 'dbTo';
        ATTACH DATABASE '$dbPathFrom' AS 'dbFrom';
      ''');

      for (final playlistUid in playlistUids) {
        dbFrom.execute('''
            INSERT INTO dbTo.${BS.remotePlaylistsTableId} 
              ( ${SqlStringMaker.remotePlaylistsColumnsWithQuotesAndCommas} )
            SELECT ${SqlStringMaker.remotePlaylistsColumnsWithCommas}
            FROM dbFrom.${BS.remotePlaylistsTableId}
            WHERE uid = '$playlistUid'
            ON CONFLICT DO UPDATE SET url=url
            ;
          ''');
      }

      dbFrom.dispose();
    });
  }
}
