// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

/// Rich text content explaining the exported text playlist.
class ExportPlaylistExplainer extends StatelessWidget {
  const ExportPlaylistExplainer({
    super.key,
    required this.filePath,
  });

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: '''
A text file will be saved to:
${filePath}


It can be used, for example, with ''',
            style: TextStyle(
                color: Theme.of(context).colorScheme.onSecondaryContainer),
          ),
          TextSpan(
            text: 'yt-dlp',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.primary,
              decoration: TextDecoration.underline,
            ),
            recognizer: TapGestureRecognizer()
              ..onTap = () async {
                if (!await launchUrl(
                    Uri.parse('https://github.com/yt-dlp/yt-dlp'))) {
                  throw Exception('https://github.com/yt-dlp/yt-dlp');
                }
              },
          ),
          TextSpan(
            text: ' to batch-download all of the videos from the playlist.',
            style: TextStyle(
                color: Theme.of(context).colorScheme.onSecondaryContainer),
          ),
        ],
      ),
    );
  }
}
