// SPDX-License-Identifier: GPL-3.0-only

import 'dart:convert';
import 'db_manager.dart';

class JsonImporter {
  static void playlistFromString(String jsonString, String dbPath) {
    final Map parsedJson = jsonDecode(jsonString);

    parsedJson.forEach((playlistName, streams) {
      print('title: ${playlistName}');
      print('items: ${streams.length}');
      print('streams.runtimeType: ${streams.runtimeType}');
      DbManager.importPlaylistFromJson(
          dbPath, playlistName as String, streams as List<dynamic>);
    });
  }
}
