// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_state.dart';
import 'constants.dart';

/// Display and control a multiple-choice, radio-button-type
/// [SharedPreferences] item.
class PreferenceItemRadioGroup extends StatelessWidget {
  const PreferenceItemRadioGroup({
    super.key,
    required this.sharedPreferences,
    required this.screenIsCompact,
    required this.title,
    required this.options,
    required this.id,
    this.description = '',
  });

  final bool screenIsCompact;
  final SharedPreferences? sharedPreferences;
  final String title;
  final String id;
  final String description;
  final Map<String, dynamic> options;

  @override
  Widget build(BuildContext context) {
    var defaultValue = BS.defaultPreferences[id];
    var layout = screenIsCompact
        ? BS.preferenceItemLayoutCompact
        : BS.preferenceItemLayoutStandard;
    var currentValue = sharedPreferences?.getString(id) ?? defaultValue;

    return sharedPreferences == null
        ? const Text('Shared preferences missing.')
        : Padding(
            padding: layout['outerPadding'],
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: layout['titlePadding'],
                    child: Text(
                      title,
                      style: const TextStyle(
                          fontSize: BS.fontSizeHeading, height: 1),
                    ),
                  ),
                ],
              ),
              if (description.isNotEmpty)
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: layout['descriptionPadding'],
                        child: Text(
                          description,
                          style: const TextStyle(
                              fontSize: BS.fontSizeSmall, height: 1),
                        ),
                      ),
                    ),
                  ],
                ),
              SizedBox(
                  height: screenIsCompact ? BS.paddingSmall : BS.paddingMedium),
              ...options.entries
                  .map<Widget>((entry) => InkWell(
                        onTap: () {
                          _setValue(entry.value);
                        },
                        child: Row(children: [
                          Radio(
                            value: entry.value,
                            groupValue: currentValue,
                            onChanged: (value) {
                              _setValue(value ?? defaultValue);
                            },
                          ),
                          SizedBox(
                            width: layout['descriptionPadding'].left,
                          ),
                          Text(entry.key)
                        ]),
                      ))
                  
            ]),
          );
  }

  /// Update the state of this preference in the app.
  void _setValue(value) {
    sharedPreferences?.setString(id, value);
    AppState.update('preferences', sharedPreferences, forceRebuild: true);
  }
}
