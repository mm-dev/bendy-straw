// SPDX-License-Identifier: GPL-3.0-only

import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import 'app_state.dart';
import 'constants.dart';
import 'db_manager.dart';
import 'json_importer.dart';

/// TODO Remove picker animation when choosing files?

/// Show a filechooser for opening/adding/saving NewPipe zips containing
/// database files.
class FileManagerWidget extends StatefulWidget {
  const FileManagerWidget({super.key});

  @override
  State<FileManagerWidget> createState() => _FileManagerWidgetState();
}

class _FileManagerWidgetState extends AppState<FileManagerWidget> {
  @override
  List<String>? listenForChanges = [
    'currentSelectedDbPath',
    'aDatabaseWasDirtied'
  ];

  // Slash to be used in file paths depending on which OS is in use
  final String _osSlash = Platform.isWindows ? '\\' : '/';

  @override
  Widget build(BuildContext context) {
    String currentSelectedDbPath = AppState.get('currentSelectedDbPath');
    bool areUnsavedChanges = currentSelectedDbPath.isNotEmpty &&
        DbManager.getDbDirtyState(currentSelectedDbPath);
    bool showJsonImportButton =
        AppState.get('preferences')?.getBool('showJsonImportButton') ??
            BS.defaultPreferences['showJsonImportButton'];

    ThemeData dbThemeData = currentSelectedDbPath.isEmpty
        ? Theme.of(context)
        : DbManager.getDbCurrentThemeData(currentSelectedDbPath, context);
    Color dbBackgroundColor = dbThemeData.colorScheme.surface;
    Color dbForegroundColor = dbThemeData.colorScheme.onSurface;
    Color dbBackgroundAttentionColor = dbThemeData.colorScheme.primary;
    Color dbForegroundAttentionColor = dbThemeData.colorScheme.onPrimary;

    return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Tooltip(
          message: 'Add file',
          child: FloatingActionButton(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                topRight: Radius.circular(BS.cornerButtonRadius),
              )),
              backgroundColor: Theme.of(context).colorScheme.surface,
              foregroundColor: Theme.of(context).colorScheme.onSurface,
              heroTag: null,
              elevation: BS.highButtonElevation,
              onPressed: _pickZipDbFile,
              child: const Icon(Icons.add_to_photos_rounded)),
        ),
        if (showJsonImportButton && currentSelectedDbPath.isNotEmpty)
          Tooltip(
            message: 'Import JSON',
            child: FloatingActionButton(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(BS.cornerButtonRadius),
                  topRight: Radius.circular(BS.cornerButtonRadius),
                )),
                backgroundColor: Theme.of(context).colorScheme.surface,
                foregroundColor: Theme.of(context).colorScheme.onSurface,
                heroTag: null,
                elevation: BS.highButtonElevation,
                onPressed: _pickJsonPlaylistFile,
                child: const Icon(Icons.code_rounded)),
          ),
        areUnsavedChanges
            ? Tooltip(
                message: 'Export file',
                child: FloatingActionButton(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(BS.cornerButtonRadius),
                    )),
                    backgroundColor: dbBackgroundAttentionColor,
                    foregroundColor: dbForegroundAttentionColor,
                    heroTag: null,
                    elevation: BS.highButtonElevation,
                    onPressed: _saveFile,
                    child: const Icon(Icons.file_download_rounded)),
              )
            : Tooltip(
                message: '(No unsaved changes)',
                child: FloatingActionButton(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(BS.cornerButtonRadius),
                    )),
                    backgroundColor: dbBackgroundColor,
                    foregroundColor: dbForegroundColor,
                    heroTag: null,
                    elevation: BS.highButtonElevation,
                    onPressed: null,
                    child: const Opacity(
                        opacity: BS.disableControlOpacity,
                        child: Icon(Icons.file_download_rounded))),
              ),
      ])
    ]);
  }

  void _pickZipDbFile() async {
    String? pickedPath = await _pickFile('Open a NewPipe zip file', ['.zip']);
    String? pickedPathSafe = pickedPath;
    if (pickedPath != null) {
      /// Windows ends up with additional drive letters eg 'C:' in the temporary
      /// path, so replace the colons with a string which is valid in Windows,
      /// but hopefully won't occur in any filepaths we encounter.
      if (Platform.isWindows) {
        pickedPathSafe = pickedPath.replaceAll(':', '()_-,');
      }

      /// NewPipe works with (exports/imports) zip files.
      ///
      /// We have to extract the zip so `sqlite3` can access the `*.db` file.
      ///
      /// Get a temp directory from the OS, add a directory for BendyStraw,
      /// then recreate the entire path for the original file inside the
      /// directory, so eg: - original file is `/home/user1/NewPipeData.zip` -
      /// extraction directory is
      /// `/tmp/bendy-straw-unzipped/home/user1/NewPipeData.zip/`
      ///
      /// **The long recreated path is necessary in case multiple zips are
      /// opened with the same filename in different filesystem locations**
      Directory tmpDir = await getTemporaryDirectory();
      String tmpDirPath =
          '${tmpDir.path}${_osSlash}${BS.baseWorkingDirectory}${_osSlash}${pickedPathSafe}';
      AppState.debug('tmpDirPath: $tmpDirPath');

      /// Extract zip to [tmpDirPath].
      final inputStream = InputFileStream(pickedPath);
      final archive = ZipDecoder().decodeBuffer(inputStream);
      await extractArchiveToDisk(archive, tmpDirPath);

      /// Path to the extracted database file.
      String extractedDbPath = '${tmpDirPath}${_osSlash}newpipe.db';
      AppState.debug('extractedDbPath: $extractedDbPath');

      /// Add the extracted database to the app.
      /// The full filepath is used as a key in a [Map].
      /// Sometimes no db will be added (eg if the same file is added twice).
      if (DbManager.addDbByPathKey(extractedDbPath, pickedPath)) {
        AppState.update('databases', DbManager.databases, forceRebuild: true);
      }
    }
  }

  void _pickJsonPlaylistFile() async {
    String? pickedPath =
        await _pickFile('Open a JSON playlist file', ['.json']);

    if (pickedPath != null) {
      File(pickedPath).readAsString().then((String jsonString) {
        JsonImporter.playlistFromString(
            jsonString, AppState.get('currentSelectedDbPath'));
      });
    }
  }

  Future<String?> _pickFile(
      String title, List<String> allowedExtensions) async {
    List<String> systemPaths = Platform.isLinux
        ? BS.filepathsLinux
        : Platform.isWindows
            ? BS.filepathsWindows
            : BS.filepathsAndroid;

    String? pickedPath = await FilesystemPicker.open(
      title: title,
      context: context,
      shortcuts: systemPaths
          .map<FilesystemPickerShortcut>(
            (path) => FilesystemPickerShortcut(
                name: path, path: Directory(path), icon: Icons.snippet_folder),
          )
          .toList(),
      fsType: FilesystemType.file,
      requestPermission: _checkStoragePermission,
      allowedExtensions: allowedExtensions,
      fileTileSelectMode: FileTileSelectMode.wholeTile,
    );

    if (pickedPath == null) {
      AppState.debug('No file selected');
    }

    return pickedPath;
  }

  /// For one database: Bundle up its extracted files from the original zip
  /// (including the updated database) from the temp directory. Write the
  /// updated zip back to the same directory as the original.
  void _saveFile() {
    /// Get the full path for currently selected database/tab, and trim off the
    /// file part to leave us with the directory.
    String currentSelectedDbPath = AppState.get('currentSelectedDbPath');

    String updatedDbDirectory =
        currentSelectedDbPath.replaceAll('newpipe.db', '');
    AppState.debug('updatedDbDirectory: $updatedDbDirectory');

    String outputZipPath = AppState.get('databases')[currentSelectedDbPath]
            ['originalZipPath']
        .replaceAll('.zip', '${BS.savedFileSuffix}.zip');
    AppState.debug('outputZipPath: $outputZipPath');

    var encoder = ZipFileEncoder();
    encoder.zipDirectory(Directory(updatedDbDirectory),
        filename: outputZipPath);

    DbManager.setDbDirtyState(AppState.get('currentSelectedDbPath'), false);

    AppState.showAppDialog(title: 'File saved:', message: '''
A new file has been saved to:
$outputZipPath


You can import it into NewPipe now.

Don't delete your old file until you're sure everything is ok with the new one!
    ''');
  }

  /// Passed into [FilesystemPicker.open()] via its [requestPermission]
  /// parameter, to be used when it wants to check that we have permission to
  /// read files on this system.
  Future<bool> _checkStoragePermission() async {
    bool isPermitted = true;
    PermissionStatus status;
    if (Platform.isAndroid) {
      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      final AndroidDeviceInfo info = await deviceInfoPlugin.androidInfo;
      if ((info.version.sdkInt) >= 30) {
        status = await Permission.manageExternalStorage.request();
      } else {
        status = await Permission.storage.request();
      }
      if (status != PermissionStatus.granted) {
        isPermitted = false;
        AppState.debug('''
BendyStraw needs storage permissions to work.

status: $status
''');
      }
      AppState.debug('status: $status');
    }
    AppState.debug('isPermitted: $isPermitted');

    return isPermitted;
  }
}
