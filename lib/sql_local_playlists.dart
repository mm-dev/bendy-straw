// SPDX-License-Identifier: GPL-3.0-only

import 'package:sqlite3/sqlite3.dart';

import 'constants.dart';
import 'enum.dart';
import 'sql_helpers.dart';
import 'sql_string_maker.dart';

/// Low level SQL functions to operate on local playlists.
///
/// Only [DbManager] should call functions directly from this class. Other
/// classes should call  wrapper functions in [DbManager].
class SqlLocalPlaylists {
  static void renameLocalPlaylist(dbPath, playlistUid, newName) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      db.execute('''
        UPDATE ${BS.localPlaylistsTableId}
          SET name = '${newName}'
          WHERE uid = '${playlistUid}';
      ''');
      db.dispose();
    });
  }

  /// Delete a local playlist.
  static void deleteLocalPlaylist(dbPath, playlistUid) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      db.execute('''
        DELETE FROM ${BS.localPlaylistsTableId}
        WHERE uid = '${playlistUid}';
      ''');
      db.dispose();
    });
  }

  /// Delete streams from a local playlist.
  static void deleteItemsFromLocalPlaylist(dbPath, playlistUid, streamUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      for (final streamUid in streamUids) {
        /// Store current join index from before we delete the row
        int joinIndex = db.select('''
          DELETE FROM ${BS.joinTableId}
          WHERE stream_id = '${streamUid}'
            AND playlist_id = '${playlistUid}'
          RETURNING join_index;
        ''')[0]['join_index'];
        print('joinIndex: $joinIndex');

        /// Decrement any higher [join_index] of other streams in this playlist to fill in the gap it left
        db.execute('''
          UPDATE ${BS.joinTableId}
            SET join_index = join_index - 1
            WHERE playlist_id = ${playlistUid}
              AND join_index > ${joinIndex}
              AND join_index > 0;
        ''');
      }
      db.dispose();
    });
  }

  /// Reverse the order of streams in a local playlist.
  static void reverseLocalPlaylistOrder(dbPath, playlistUid) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);

      ResultSet playlistStreamJoinResultSet = db.select('''
        SELECT stream_id FROM ${BS.joinTableId}
          WHERE playlist_id = '${playlistUid}'
          ORDER BY join_index
      ''');

      List streamUidsReversed = playlistStreamJoinResultSet
          .map((row) => row['stream_id'])
          .toList()
          .reversed
          .toList();

      SqlLocalPlaylists.reOrderLocalPlaylist(
          dbPath, playlistUid, streamUidsReversed);

      db.dispose();
    });
  }

  /// Shuffle the order of streams in a local playlist.
  static void shuffleLocalPlaylistOrder(dbPath, playlistUid) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);

      ResultSet playlistStreamJoinResultSet = db.select('''
        SELECT stream_id FROM ${BS.joinTableId}
          WHERE playlist_id = '${playlistUid}'
          ORDER BY join_index
      ''');

      List streamUidsShuffled = playlistStreamJoinResultSet
          .map((row) => row['stream_id'])
          .toList();
      streamUidsShuffled.shuffle();

      SqlLocalPlaylists.reOrderLocalPlaylist(
          dbPath, playlistUid, streamUidsShuffled);

      db.dispose();
    });
  }

  /// Copy streams from one local playlist to another.
  static void copyItemsFromLocalPlaylist(
      dbPathFrom, dbPathTo, playlistUidFrom, playlistUidTo, streamUids) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database dbFrom = sqlite3.open(dbPathFrom);
      final Database dbTo = sqlite3.open(dbPathTo);

      /// Attach both databases with aliases so we can be clear which we're
      /// dealing with. Attaching dbFrom to itself seems strange but the alias
      /// is useful for clarity.
      dbFrom.execute('''
        ATTACH DATABASE '$dbPathTo' AS 'dbTo';
        ATTACH DATABASE '$dbPathFrom' AS 'dbFrom';
      ''');

      /// Get joined streams and playlists
      createPlaylistStreamJoinTable(dbFrom);
      ResultSet playlistStreamSelectionJoinResultSet = dbFrom.select('''
        SELECT * FROM ${BS.tempPlaylistStreamJoinedTableId}
        WHERE stream_uid IN (${streamUids.map((e) => '\'$e\'').toList().join(", ")})
          AND playlist_uid = '${playlistUidFrom}'
      ''');

      /// Insert streams and store their UIDs.
      /// When combining playlists eg from two devices, we don't want duplicate streams
      /// so see if a stream with the same URL already exists and if it does use its UID
      final PreparedStatement streamInsertStatement = dbFrom.prepare('''
            INSERT INTO dbTo.${BS.streamsTableId} ( ${SqlStringMaker.streamColumnsWithQuotesAndCommas} )
            VALUES ( ${SqlStringMaker.streamColumnsUnspecifiedParams} )
            ON CONFLICT DO NOTHING RETURNING uid
            ;
          ''');
      List<int> newStreamUids = [];
      for (final row in playlistStreamSelectionJoinResultSet) {
        ResultSet uidResultSet = streamInsertStatement.select(StreamColumns
            .values
            .map((e) => SqlHelpers.escapeQuotes(row[e.name]))
            .toList());
        newStreamUids.add(uidResultSet[0]['uid']);
      }

      /// !!! first get the highest index for this playlist !!!
      /// For the starting join_index (so we add our streams to the end of the
      /// playlist).
      int joinIndex =
          SqlLocalPlaylists.getHighestJoinIndex(dbTo, playlistUidTo) + 1;

      // Fill in the join table. Join index increments with each added stream.
      /// Don't insert duplicates where playlistid and streamid are both matched
      for (final newStreamUid in newStreamUids) {
        ResultSet alreadyExistsRecordSet = dbFrom.select('''
          SELECT * FROM dbTo.${BS.joinTableId}
          WHERE stream_id = ${newStreamUid}
            AND playlist_id = ${playlistUidTo}
        ''');
        if (alreadyExistsRecordSet.isEmpty) {
          dbFrom.execute('''
            INSERT INTO dbTo.${BS.joinTableId} (
              'playlist_id', 'stream_id', 'join_index'
            )
            VALUES ( ${playlistUidTo}, ${newStreamUid}, ${joinIndex} );
          ''');
        }
        joinIndex++;
      }

      dbFrom.dispose();
      dbTo.dispose();
    });
  }

  /// Copy a local playlist from one database to another.
  static void copyLocalPlaylist(dbPathFrom, dbPathTo, playlistUid) {
    SqlHelpers.tryAndCatchErrors(() {
      final Database dbFrom = sqlite3.open(dbPathFrom);

      /// Attach both databases with aliases so we can be clear which we're
      /// dealing with. Attaching dbFrom to itself seems strange but the alias
      /// is useful for clarity in the SQL queries.
      dbFrom.execute('''
        ATTACH DATABASE '$dbPathTo' AS 'dbTo';
        ATTACH DATABASE '$dbPathFrom' AS 'dbFrom';
      ''');

      /// Get joined streams and playlists
      createPlaylistStreamJoinTable(dbFrom);
      ResultSet playlistStreamJoinResultSet = dbFrom.select('''
        SELECT *
        FROM ${BS.tempPlaylistStreamJoinedTableId}
        WHERE playlist_uid = '${playlistUid}'
      ''');

      /// Insert streams and store their UIDs.
      /// Stream UIDs must be unique so trying to insert one that exists results
      /// in a conflict. When this happens, just get the existing UID (via `ON
      /// CONFLICT`).
      final PreparedStatement streamInsertStatement = dbFrom.prepare('''
          INSERT INTO dbTo.${BS.streamsTableId} ( ${SqlStringMaker.streamColumnsWithQuotesAndCommas} )
          VALUES ( ${SqlStringMaker.streamColumnsUnspecifiedParams} )
          ON CONFLICT DO NOTHING RETURNING uid
          ;
        ''');
      List<int> newStreamUids = [];
      for (final row in playlistStreamJoinResultSet) {
        ResultSet uidResultSet = streamInsertStatement.select(StreamColumns
            .values
            .map((e) => SqlHelpers.escapeQuotes(row[e.name]))
            .toList());
        newStreamUids.add(uidResultSet[0]['uid']);
      }
      print('newStreamUids: $newStreamUids');

      /// Add new playlist entry
      dbFrom.execute('''
        INSERT INTO dbTo.${BS.localPlaylistsTableId}
          ( name, is_thumbnail_permanent, thumbnail_stream_id )
        SELECT name, is_thumbnail_permanent, thumbnail_stream_id
          FROM dbFrom.${BS.localPlaylistsTableId} WHERE uid = '${playlistUid}';
       ;
      ''');

      /// Although we insert into [dbTo], it's attached to [dbFrom] so the `lastInsertRowId` comes from that context.
      int newPlaylistUid = dbFrom.lastInsertRowId;
      print('newPlaylistUid: $newPlaylistUid');

      // Fill in the join table. Join index increments with each added stream.
      int joinIndex = 0;
      for (final newStreamUid in newStreamUids) {
        dbFrom.execute('''
          INSERT INTO dbTo.${BS.joinTableId} 
            ( 'playlist_id', 'stream_id', 'join_index' )
          VALUES ( ${newPlaylistUid}, ${newStreamUid}, ${joinIndex} );
        ''');
        joinIndex++;
      }

      dbFrom.dispose();
    });
  }

  /// Create the special joined table which allows us to link playlists
  /// and streams.
  /// This doesn't need to be wrapped in [SqlHelpers.tryAndCatchErrors()] as
  /// the caller function already is.
  static createPlaylistStreamJoinTable(Database db) {
    db.execute('''
      CREATE TEMP TABLE ${BS.tempPlaylistStreamJoinedTableId} AS
        SELECT
          url,
          title,
          stream_type,
          duration,
          uploader,
          ${BS.localPlaylistsTableId}.name
            AS playlist_name,
          ${BS.streamsTableId}.uid
            AS stream_uid,
          ${BS.localPlaylistsTableId}.uid
            AS playlist_uid
        FROM ${BS.streamsTableId}
        INNER JOIN ${BS.joinTableId}
          ON ${BS.joinTableId}.stream_id = ${BS.streamsTableId}.uid
        INNER JOIN ${BS.localPlaylistsTableId}
          ON ${BS.localPlaylistsTableId}.uid = ${BS.joinTableId}.playlist_id
    ''');
  }

  /// Process playlist data (as stored in the database) into something
  /// (a [Map]) easier to work with.
  ///
  /// NewPipe stores playlists and their individual streams (eg video/audio URLS) in separate tables. Join them together here and convert them into [Map]s which we can work with more easily.
  static Map<String, Map> getLocalPlaylistsMap(dbPath) {
    Map<String, Map> localPlaylists = Map<String, Map>.from({});

    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);

      /// Create the temporary table with all the data we need joined together.
      createPlaylistStreamJoinTable(db);

      /// Select only the unique playlist names from the temporary table.
      /// Each stream stores its playlist name in a column in the temporary joined table, so they are repeated lots of times.
      /// Select in case-insensitive, alphabetical order by 'playlist_name'
      List<Map> localPlaylistsMetaData = db
          .select('''
            SELECT DISTINCT	playlist_name, playlist_uid
            FROM ${BS.tempPlaylistStreamJoinedTableId}
            ORDER BY playlist_name COLLATE NOCASE
            ;
          ''')
          .rows
          .map((value) =>
              {'name': value[0].toString(), 'uid': value[1].toString()})
          .toList();

      /// Create a [Map] of data for each custom playlist in [dbPath]'s database and store them in a [localPlaylists] container
      for (Map metaData in localPlaylistsMetaData) {
        String columnList = LocalPlaylistColumns.values
            .asNameMap()
            .entries
            .map((e) => '${e.key} AS ${e.value.displayName}')
            .toList()
            .join(', ');
        Map meta = localPlaylists[metaData['uid']] = {};
        meta['displayName'] = metaData['name'];

        /// This is actually a [ResultSet], which contains ['rows'], so needs to change name
        meta['rows'] = db.select('''
          SELECT ${columnList}
          FROM ${BS.tempPlaylistStreamJoinedTableId}
          WHERE playlist_uid = '${metaData['uid']}'
      ''');
      }
      // Finish up
      db.dispose();
    });

    return localPlaylists;
  }

  /// Rewrite a local playlist in a different order.
  static void reOrderLocalPlaylist(dbPath, playlistUid, streamUidsInNewOrder) {
    print('streamUidsInNewOrder: $streamUidsInNewOrder');
    int joinIndex = 0;
    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);
      // Delete old join info as we'll rewrite it.
      for (final streamUid in streamUidsInNewOrder) {
        db.execute('''
          DELETE FROM ${BS.joinTableId}
            WHERE stream_id = '${streamUid}'
              AND playlist_id = '${playlistUid}';
        ''');
      }

      // Re-enter the streams into the join table. Join index increments with each added stream.
      for (final streamUid in streamUidsInNewOrder) {
        db.execute('''
          INSERT INTO ${BS.joinTableId} 
            ( 'playlist_id', 'stream_id', 'join_index' )
          VALUES 
            ( ${playlistUid}, ${streamUid}, ${joinIndex} );
        ''');
        joinIndex++;
      }

      db.dispose();
    });
  }

  static void importPlaylistFromJson(
      String dbPath, String playlistName, List<dynamic> streams) {
    //print('importPlaylistFromJson()::');
    int joinIndex = 0;

    SqlHelpers.tryAndCatchErrors(() {
      final Database db = sqlite3.open(dbPath);

      /// Try to get guid for the playlist from 'playlists' table
      /// If playlist with the same name exists:
      String? playlistUid =
          SqlLocalPlaylists.getPlaylistUidFromName(db, playlistName);

      if (playlistUid == null) {
        /// Is a new playlist:
        /// - Leave joinIndex as 0
        /// - Insert a new record in 'playlists' table ('name', 0, -1) and get the guid back from the insert
        db.execute('''
          INSERT INTO ${BS.localPlaylistsTableId}
            ( name)
          VALUES 
            ( '${playlistName}' );
        ''');
        playlistUid = db.lastInsertRowId.toString();
      } else {
        /// Playlist with this name already exists:
        /// - Set joinIndex to the highest join index + 1
        joinIndex = SqlLocalPlaylists.getHighestJoinIndex(db, playlistUid) + 1;
      }

      Map streamInsertDefaults = {
        'service_id': '0',
        'duration': '0',
        'view_count': '-1',
        'uploader_url': '',
        'thumbnail_url': '',
      };
      final PreparedStatement streamInsertStatement = db.prepare('''
        INSERT INTO ${BS.streamsTableId} (
            'service_id',
            'duration',
            'view_count',
            'url',
            'title',
            'stream_type',
            'uploader',
            'uploader_url'
          )
        VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )
        ON CONFLICT DO NOTHING RETURNING uid
        ;
      ''');

      /// Loop through each stream, for each:
      for (Map stream in streams) {
        /// Add any missing properties from defaults
        streamInsertDefaults.forEach((key, value) {
          if (!stream.containsKey(key)) {
            stream[key] = value;
          }
        });

        /// - if stream.url exists in streams table, get the uid for the existing stream
        /// - otherwise, insert the stream into the streams table, and get the returned uid
        ResultSet uidResultSet = streamInsertStatement.select([
          SqlHelpers.escapeQuotes(stream['service_id']),
          SqlHelpers.escapeQuotes(stream['duration']),
          SqlHelpers.escapeQuotes(stream['view_count']),
          SqlHelpers.escapeQuotes(stream['url']),
          SqlHelpers.escapeQuotes(stream['title']),
          SqlHelpers.escapeQuotes(stream['stream_type']),
          SqlHelpers.escapeQuotes(stream['uploader']),
          SqlHelpers.escapeQuotes(stream['uploader_url'])
        ]);
        String? streamUid = uidResultSet[0]['uid'].toString();

        /// - Insert into 'playlist_stream_join' (playlist_id, stream_id, join_index)
        db.execute('''
            INSERT INTO ${BS.joinTableId} (
              'playlist_id', 'stream_id', 'join_index'
            )
            VALUES ( ${playlistUid}, ${streamUid}, ${joinIndex} );
          ''');
        joinIndex++;
      }

      db.dispose();
    });
  }

  /// Get the UID for an existing playlist with a specific name
  static String? getStreamUidFromUrl(Database db, String streamUrl) {
    String? urlMatchUid;

    ResultSet streamUidResultSet = db.select('''
        SELECT uid FROM ${BS.streamsTableId}
        WHERE url = '${streamUrl}';
      ''');

    if (streamUidResultSet.isNotEmpty) {
      urlMatchUid = streamUidResultSet[0][0].toString();
    }

    return urlMatchUid;
  }

  /// Get the UID for an existing playlist with a specific name
  static String? getPlaylistUidFromName(Database db, String playlistName) {
    String? nameMatchUid;

    ResultSet playlistUidResultSet = db.select('''
        SELECT uid FROM ${BS.localPlaylistsTableId}
        WHERE name = '${playlistName}';
      ''');

    if (playlistUidResultSet.isNotEmpty) {
      nameMatchUid = playlistUidResultSet[0][0].toString();
    }

    return nameMatchUid;
  }

  /// Get the highest join index for a specific playlist
  static int getHighestJoinIndex(Database db, String playlistUid) {
    int maxJoinIndex = 0;

    ResultSet maxJoinIndexResultSet = db.select('''
        SELECT MAX(join_index) FROM ${BS.joinTableId}
        WHERE playlist_id = '${playlistUid}';
      ''');

    if (maxJoinIndexResultSet.isNotEmpty) {
      maxJoinIndex = maxJoinIndexResultSet[0]['MAX(join_index)'] ?? 0;
    }

    return maxJoinIndex;
  }
}
