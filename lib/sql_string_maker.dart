// SPDX-License-Identifier: GPL-3.0-only

import 'enum.dart';

/// Static functions to return strings for use in SQL queries.
class SqlStringMaker {
  static String streamColumnsWithQuotesAndCommas =
      StreamColumns.values.map((e) => "'${e.name}'").toList().join(', ');

  static String streamColumnsUnspecifiedParams =
      StreamColumns.values.map((e) => '?').toList().join(', ');

  static String remotePlaylistsColumnsWithQuotesAndCommas =
      RemotePlaylistsColumns.values
          .where((e) => !e.isPrimaryKey)
          .map((e) => "'${e.name}'")
          .join(', ');

  static String remotePlaylistsColumnsWithCommas = RemotePlaylistsColumns.values
      .where((e) => !e.isPrimaryKey)
      .map((e) => e.name)
      .join(', ');

  static String subscriptionsColumnsWithQuotesAndCommas = SubscriptionsColumns
      .values
      .where((e) => !e.isPrimaryKey)
      .map((e) => "'${e.name}'")
      .join(', ');

  static String subscriptionsColumnsWithCommas = SubscriptionsColumns.values
      .where((e) => !e.isPrimaryKey)
      .map((e) => e.name)
      .join(', ');
}
