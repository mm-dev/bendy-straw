// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';
import 'screen.dart';

/// A table cell specialised for showing the duration of a stream.
class TableCellDuration extends StatelessWidget {
  final dynamic value;

  const TableCellDuration({
    super.key,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    Map cellLayout = Screen.isCompact(context)
        ? BS.tableCellLayoutCompact
        : BS.tableCellLayoutStandard;
    return Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: cellLayout['padding'],
          child: Text(
              '${(Duration(seconds: value))}'.split('.')[0].padLeft(8, '0'),
              style: cellLayout['textStyle']),
        ));
  }
}
