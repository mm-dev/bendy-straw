// SPDX-License-Identifier: GPL-3.0-only

import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';

import 'table_cell_default.dart';
import 'table_cell_duration.dart';
import 'table_cell_numeric_compact.dart';
import 'table_cell_url.dart';

// We want to use the exact column names as per the sqlite database, so ignore
// warnings about not using camelcase.
// ignore_for_file: constant_identifier_names

/// Describe columns for local (custom) playlists
enum LocalPlaylistColumns {
  title(
      displayName: 'Title',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  uploader(
      displayName: 'Channel',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  url(
      displayName: 'URL',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: false),
  duration(
      displayName: 'Length',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  stream_uid(
      displayName: 'UID',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: true);

  final String displayName;
  final ColumnSize displaySize;
  final bool numeric;
  final bool isVisible;
  final bool canSortOn;
  final bool isPrimaryKey;

  const LocalPlaylistColumns({
    required this.displayName,
    required this.displaySize,
    required this.numeric,
    required this.isVisible,
    required this.canSortOn,
    required this.isPrimaryKey,
  });

  Widget getContentWidget(value) {
    late final Widget widget;
    switch (this) {
      case duration:
        widget = TableCellDuration(value: value);
      case url:
        widget = TableCellUrl(value: value);
      default:
        widget = TableCellDefault(numeric: numeric, value: value);
    }
    return widget;
  }

  //int getPrimaryKeyColumnIndex() {
  //  int primaryKeyColumnIndex = -1;
  //  for (var i = 0; i < LocalPlaylistColumns.values.length; i++) {
  //    if (LocalPlaylistColumns.values[i].isPrimaryKey) {
  //      primaryKeyColumnIndex = i;
  //      break;
  //    }
  //  }
  //  return primaryKeyColumnIndex;
  //}
}

/// Describe columns for remote playlists (those which are made/controlled by
/// people other than the user).
enum RemotePlaylistsColumns {
  name(
      displayName: 'Name',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  uploader(
      displayName: 'Channel',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  url(
      displayName: 'URL',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: false),
  stream_count(
      displayName: 'Items',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  uid(
      displayName: 'UID',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: true),
  service_id(
      displayName: 'service_id',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: false,
      canSortOn: false,
      isPrimaryKey: false),
  thumbnail_url(
      displayName: 'thumbnail_url',
      displaySize: ColumnSize.S,
      numeric: false,
      isVisible: false,
      canSortOn: false,
      isPrimaryKey: false);

  final String displayName;
  final ColumnSize displaySize;
  final bool numeric;
  final bool isVisible;
  final bool canSortOn;
  final bool isPrimaryKey;

  const RemotePlaylistsColumns({
    required this.displayName,
    required this.displaySize,
    required this.numeric,
    required this.isVisible,
    required this.canSortOn,
    required this.isPrimaryKey,
  });

  Widget getContentWidget(value) {
    late Widget widget;
    switch (this) {
      case url:
        widget = TableCellUrl(value: value);
      default:
        widget = TableCellDefault(numeric: numeric, value: value);
    }
    return widget;
  }
}

/// Describe columns for channel subscription tables.
enum SubscriptionsColumns {
  name(
      displayName: 'Channel',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  description(
      displayName: 'Description',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  url(
      displayName: 'URL',
      displaySize: ColumnSize.M,
      numeric: false,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: false),
  subscriber_count(
      displayName: 'Subscribers',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: true,
      isPrimaryKey: false),
  uid(
      displayName: 'UID',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: true,
      canSortOn: false,
      isPrimaryKey: true),
  service_id(
      displayName: 'service_id',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: false,
      canSortOn: false,
      isPrimaryKey: false),
  avatar_url(
      displayName: 'avatar_url',
      displaySize: ColumnSize.S,
      numeric: false,
      isVisible: false,
      canSortOn: false,
      isPrimaryKey: false),
  notification_mode(
      displayName: 'notification_mode',
      displaySize: ColumnSize.S,
      numeric: true,
      isVisible: false,
      canSortOn: false,
      isPrimaryKey: false);

  final String displayName;
  final ColumnSize displaySize;
  final bool numeric;
  final bool isVisible;
  final bool canSortOn;
  final bool isPrimaryKey;

  const SubscriptionsColumns({
    required this.displayName,
    required this.displaySize,
    required this.numeric,
    required this.isVisible,
    required this.canSortOn,
    required this.isPrimaryKey,
  });

  Widget getContentWidget(value) {
    late Widget widget;
    switch (this) {
      case subscriber_count:
        widget = TableCellNumericCompact(value: value);
      case url:
        widget = TableCellUrl(value: value);
      default:
        widget = TableCellDefault(numeric: numeric, value: value);
    }
    return widget;
  }
}

/// The streams table contains canonical info about streams, which are then
/// referred to in playlists (as opposed to the stream info being directly
/// stored in each playlist).
enum StreamColumns {
  service_id,
  url,
  title,
  stream_type,
  duration,
  uploader,
  thumbnail_url,
  view_count,
  textual_upload_date,
  upload_date,
  is_upload_date_approximation,
  uploader_url
}

enum TableType {
  remotePlaylists,
  subscriptions,
  localPlaylist,
}
