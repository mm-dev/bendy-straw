// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'constants.dart';
import 'screen.dart';

/// A default table cell ie just for showing simple text.
class TableCellDefault extends StatelessWidget {
  final bool numeric;
  final dynamic value;

  const TableCellDefault({
    super.key,
    required this.numeric,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    Map cellLayout = Screen.isCompact(context)
        ? BS.tableCellLayoutCompact
        : BS.tableCellLayoutStandard;
    return Align(
        alignment: numeric ? Alignment.centerRight : Alignment.centerLeft,
        child: Padding(
          padding: cellLayout['padding'],
          child: Text(value.toString(), style: cellLayout['textStyle']),
        ));
  }
}
