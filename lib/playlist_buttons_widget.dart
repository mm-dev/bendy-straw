// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_state.dart';
import 'constants.dart';
import 'db_manager.dart';
import 'destination_chooser.dart';
import 'enum.dart';
import 'export_playlist_explainer.dart';
import 'tooltip_icon_button.dart';
import 'screen.dart';

/// Buttons relating to a playlist as a whole.
///
/// Buttons which act on an entire playlist, such as copy/move/export. Each
/// button calls a private method to execute its action.
class PlaylistButtonsWidget extends StatelessWidget {
  final String playlistDisplayName;
  final String playlistTableId;
  final String playlistDbPath;

  const PlaylistButtonsWidget({
    super.key,
    required this.playlistDisplayName,
    required this.playlistTableId,
    required this.playlistDbPath,
  });

  @override
  Widget build(BuildContext context) {
    ButtonStyle buttonStyle =
        Screen.isCompact(context) ? BS.buttonStyleCompact : BS.buttonStyleStandard;
    bool showPlaylistShuffleButton =
        AppState.get('preferences')?.getBool('showPlaylistShuffleButton') ??
            BS.defaultPreferences['showPlaylistShuffleButton'];
    bool showInstantYoutubePlaylistButton =
        AppState.get('preferences')?.getBool('showInstantYoutubePlaylistButton') ??
            BS.defaultPreferences['showInstantYoutubePlaylistButton'];

    return ConstrainedBox(
      constraints: const BoxConstraints(
        minWidth: BS.renamableTextFieldMinWidth,
        //maxWidth: BS.renamableTextFieldMaxWidth,
      ),
      child: Padding(
        padding: EdgeInsets.only(
          top: BS.paddingSmall,
          bottom: BS.paddingSmall,
        ),
        child: Wrap(
          spacing: Screen.isCompact(context) ? BS.paddingExtraSmall : BS.paddingSmall,
          runSpacing: Screen.isCompact(context) ? BS.paddingMedium : BS.paddingSmall,
          //mainAxisSize: MainAxisSize.max,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Tooltip(
              message: 'Reverse playlist order',
              child: ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  _reversePlaylistOrder();
                },
                child: const Icon(Icons.swap_vert),
              ),
            ),
            if (showPlaylistShuffleButton)
              Tooltip(
                message: 'Shuffle playlist order',
                child: ElevatedButton(
                  style: buttonStyle,
                  onPressed: () {
                    _shufflePlaylistOrder();
                  },
                  child: const Icon(Icons.shuffle),
                ),
              ),
            if (showInstantYoutubePlaylistButton)
              Tooltip(
                message: 'Open as a YouTube temporary playlist',
                child: ElevatedButton(
                  style: buttonStyle,
                  onPressed: () async {
                    await _openAsYouTubePlaylist();
                  },
                  child: const Icon(Icons.web),
                ),
              ),
            Tooltip(
              message: 'Copy/Move playlist',
              child: ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  _confirmThenCopyPlaylist(context: context);
                },
                child: const Icon(Icons.drive_file_move_rounded),
              ),
            ),
            Tooltip(
              message: 'Export playlist',
              child: ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  _confirmThenExportPlaylist(context: context);
                },
                child: const Icon(Icons.web_stories_rounded),
              ),
            ),
            Tooltip(
              message: 'Delete playlist',
              child: ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  _confirmThenDeletePlaylist(context: context);
                },
                child: const Icon(Icons.playlist_remove_rounded),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Start the export process by displaying a dialog with a confirm button,
  /// which will export the playlist when pressed.
  void _confirmThenExportPlaylist({required BuildContext context}) {
    String filePath = DbManager.getExportLocalPlaylistFilepath(playlistDbPath, playlistTableId);
    AppState.showAppDialog(
        title: 'Export streams to text:',
        content: ExportPlaylistExplainer(filePath: filePath),
        actions: <Widget>[
          TooltipIconButton(
            text: Screen.isCompact(context) ? 'Export' : 'Export to $filePath',
            icon: const Icon(Icons.web_stories_rounded),
            layout: TooltipIconButtonLayout.iconOnRight,
            onPressedCallback: () {
              DbManager.exportLocalPlaylistToYtDlp(playlistDbPath, playlistTableId);
              Navigator.of(context).pop();
            },
          ),
        ]);
  }

  /// Start the deletion process by displaying a dialog with a confirm button,
  /// which will delete the playlist when pressed.
  void _confirmThenDeletePlaylist({required BuildContext context}) {
    AppState.showAppDialog(title: 'Delete playlist \'$playlistDisplayName\'?', actions: <Widget>[
      TooltipIconButton(
        text: Screen.isCompact(context) ? 'Delete' : 'Delete \'$playlistDisplayName\'',
        icon: const Icon(Icons.playlist_remove_rounded),
        layout: TooltipIconButtonLayout.iconOnRight,
        onPressedCallback: () {
          _deletePlaylistTableFromDb();
          Navigator.of(context).pop();
        },
      ),
    ]);
  }

  /// Start the copy/move process by displaying a dialog with a confirm button,
  /// which will copy or move the playlist when pressed.
  Future<void> _confirmThenCopyPlaylist({required BuildContext context}) async {
    bool isMoveMode = false;
    String chosenDestinationJson = '';
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return Center(
            child: SimpleDialog(
                titlePadding: EdgeInsets.zero,
                contentPadding: EdgeInsets.zero,
                children: [
                  DestinationChooser(
                      tableIdFrom: playlistTableId,
                      dbPathFrom: playlistDbPath,
                      tableTypeFrom: TableType.localPlaylist,
                      isFullTableCopy: true,
                      isMoveMode: isMoveMode,
                      moveModeSetter: (bool? value) {
                        setState(() {
                          isMoveMode = value ?? false;
                        });
                      },
                      chosenDestinationJson: chosenDestinationJson,
                      chosenDestinationJsonSetter: (String str) {
                        setState(() {
                          chosenDestinationJson = str;
                        });
                      },
                      chosenDestinationJsonGetter: () {
                        return chosenDestinationJson;
                      },
                      copyFunction: _copyPlaylist)
                ]),
          );
        });
      },
    );
  }

  /// Call a function to copy the playlist, then force rebuild of relevant
  /// widgets to reflect changes.
  void _copyPlaylist(
      {required String dbPathFrom,
      required String playlistUidFrom,
      required String dbPathTo,
      bool isMoveMode = false}) {
    DbManager.copyLocalPlaylist(dbPathFrom, dbPathTo, playlistUidFrom, isMoveMode);

    AppState.update(playlistDbPath, DbManager.databases, forceRebuild: true);
    print('Copy $playlistDbPath.$playlistTableId to $dbPathTo.$playlistTableId');
  }

  /// Call a function to delete the playlist, then force rebuild of relevant
  /// widgets to reflect that the table is now gone.
  void _deletePlaylistTableFromDb() {
    DbManager.deleteLocalPlaylist(playlistDbPath, playlistTableId);

    AppState.update(playlistDbPath, DbManager.databases, forceRebuild: true);
  }

  /// Call a function to reverse the order of the items in a playlist, then
  /// force rebuild of relevant widgets to reflect the changes.
  void _reversePlaylistOrder() {
    DbManager.reverseLocalPlaylistOrder(playlistDbPath, playlistTableId);

    AppState.update(playlistDbPath, DbManager.databases, forceRebuild: true);
  }

  /// Call a function to shuffle the order of the items in a playlist, then
  /// force rebuild of relevant widgets to reflect the changes.
  void _shufflePlaylistOrder() {
    DbManager.shuffleLocalPlaylistOrder(playlistDbPath, playlistTableId);

    AppState.update(playlistDbPath, DbManager.databases, forceRebuild: true);
  }

  Future<void> _openAsYouTubePlaylist() async {
    List<String> ids = DbManager.getLocalPlaylistAsYouTubeIdList(playlistDbPath, playlistTableId);
    String title = DbManager.getLocalPlaylistName(playlistDbPath, playlistTableId);
    Uri uri =
        Uri.parse("https://www.youtube.com/watch_videos?video_ids=${ids.join(',')}&title=${title}");
    print(uri.toString());
    if (!await launchUrl(uri)) {
      throw Exception(uri.toString());
    }
  }
}
