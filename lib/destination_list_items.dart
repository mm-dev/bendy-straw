// SPDX-License-Identifier: GPL-3.0-only

import 'dart:convert';

import 'package:flutter/material.dart';

import 'constants.dart';
import 'db_manager.dart';
import 'enum.dart';

/// A list of potential destinations (playlists) within a specific database.
class DestinationListItems extends StatelessWidget {
  final String tableId;
  final String dbPath;
  final TableType tableType;
  final String destinationDbPath;
  final Function chosenDestinationJsonGetter;
  final Function chosenDestinationJsonSetter;
  final Iterable localPlaylists;

  const DestinationListItems({
    super.key,
    required this.tableId,
    required this.dbPath,
    required this.tableType,
    required this.destinationDbPath,
    required this.chosenDestinationJsonGetter,
    required this.chosenDestinationJsonSetter,
    required this.localPlaylists,
  });

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: DbManager.getDbCurrentThemeData(destinationDbPath, context),
      child: Builder(builder: (context) {
        return ClipRRect(
                      borderRadius: BorderRadius.circular(BS.cornerRadius),
          child: ColoredBox(
            color: Theme.of(context).colorScheme.primaryContainer,
            child: Padding(
              padding: const EdgeInsets.all(BS.paddingSmall),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: BS.paddingSmall,
                        bottom: BS.paddingSmall,
                        left: BS.paddingSmall
                        ),
                    child: Text(DbManager.getPrettyName(destinationDbPath),
                        softWrap: false,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis),
                  ),
                  Flexible(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(BS.cornerRadius),
                      child: ColoredBox(
                        color: Theme.of(context).colorScheme.surface,
                        child: SingleChildScrollView(
                            child: Flex(direction: Axis.vertical, children: [
                          // This switch statement returns a List, so expand it.
                          ...switch (tableType) {
                            TableType.remotePlaylists => [
                                _getButton(
                                    title: BS.knownTables[tableId]?['displayName'],
                                    context: context)
                              ],
                            TableType.subscriptions => [
                                _getButton(
                                    title: BS.knownTables[tableId]?['displayName'],
                                    context: context)
                              ],
                            TableType.localPlaylist => localPlaylists.isEmpty
                                ? [
                                    _getButton(
                                        title:
                                            'Create \'${DbManager.getLocalPlaylistName(dbPath, tableId)}\'',
                                        context: context)
                                  ]
                                : localPlaylists
                                    // Don't show the playlist we're copying from
                                    // as a destination.
                                    .where((playlist) =>
                                        (!(playlist['id'] == tableId &&
                                            destinationDbPath == dbPath)))
                                    .map<Widget>((playlist) => _getButton(
                                          localPlaylistId: playlist['id'],
                                          context: context,
                                          title: DbManager.getLocalPlaylistName(
                                              destinationDbPath, playlist['id']),
                                        ))
                                    .toList()
                          }
                        ])),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  /// Get a [RadioListTile] button describing a destination playlist.
  Widget _getButton(
      {required String title,
      required BuildContext context,
      String? localPlaylistId}) {
    String destinationPathJson = jsonEncode({
      'dbPath': destinationDbPath,
      if (localPlaylistId != null)
        'playlistId': switch (tableType) {
          TableType.remotePlaylists => tableId,
          TableType.subscriptions => tableId,
          TableType.localPlaylist => localPlaylistId,
        }
    });

    return RadioListTile(
      tileColor: Colors.transparent,
      dense: true,
      title: Text(
        title,
        style: TextStyle(color: Theme.of(context).colorScheme.primary),
      ),
      value: destinationPathJson,
      groupValue: chosenDestinationJsonGetter(),
      onChanged: (value) {
        chosenDestinationJsonSetter(value ?? '');
      },
    );
  }
}
