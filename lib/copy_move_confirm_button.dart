// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

import 'screen.dart';
import 'constants.dart';
import 'tooltip_icon_button.dart';

/// 'Confirm' button for copy/move operations.
///
/// - Will be shown with different labels depending on whether this is a 'copy' or a 'move' operation
/// - Contains a toggle for 'move'/'copy' modes, which when pressed with fire a callback `onMoveModeChanged()`
/// - When the main button is pressed (so the copy or move is being confirmed), `onConfirmPressedCallback()` is called
class CopyMoveConfirmButton extends StatelessWidget {
  final String buttonLabel;
  final bool isMoveMode;
  final Function(bool?) onMoveModeChanged;
  final Function()? onConfirmPressedCallback;

  const CopyMoveConfirmButton({
    super.key,
    required this.buttonLabel,
    required this.isMoveMode,
    required this.onMoveModeChanged,
    required this.onConfirmPressedCallback,
  });

  @override
  Widget build(BuildContext context) {
    bool screenIsCompact = Screen.isCompact(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: CheckboxListTile(
              dense: screenIsCompact,
              contentPadding: screenIsCompact
                  ? const EdgeInsets.all(BS.paddingExtraSmall)
                  : const EdgeInsets.all(BS.paddingMedium),
              title: ConstrainedBox(
                constraints: const BoxConstraints(
                  minWidth: BS.moveModeTextMinWidth,
                  maxHeight: BS.moveModeTextMaxHeight,
                ),
                child: const Text(
                  'Move mode',
                  style: TextStyle(fontSize: BS.fontSizeSmall, height: 1),
                ),
              ),
              value: isMoveMode,
              onChanged: onMoveModeChanged,
              controlAffinity: ListTileControlAffinity.leading),
        ),
        Flexible(
          flex: 0,
          child: TooltipIconButton(
            text: buttonLabel,
            icon: const Icon(Icons.drive_file_move_rounded),
            layout: TooltipIconButtonLayout.iconOnRight,
            onPressedCallback: onConfirmPressedCallback,
          ),
        )
      ],
    );
  }
}
