// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_state.dart';
import 'constants.dart';

/// Display and control a togglable [SharedPreferences] item.
class PreferenceItemBoolean extends StatelessWidget {
  const PreferenceItemBoolean({
    super.key,
    required this.sharedPreferences,
    required this.screenIsCompact,
    required this.title,
    required this.id,
    this.description = '',
  });

  final bool screenIsCompact;
  final SharedPreferences? sharedPreferences;
  final String title;
  final String id;
  final String description;

  @override
  Widget build(BuildContext context) {
    var defaultValue = BS.defaultPreferences[id];
    var layout = screenIsCompact
        ? BS.preferenceItemLayoutCompact
        : BS.preferenceItemLayoutStandard;
    var currentValue = sharedPreferences?.getBool(id) ?? defaultValue;

    return sharedPreferences == null
        ? const Text('Shared preferences missing.')
        : InkWell(
            onTap: () {
              _setValue(!currentValue);
            },
            child: Padding(
              padding: layout['outerPadding'],
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Checkbox(
                    checkColor: Theme.of(context).colorScheme.onPrimary,
                    value: currentValue,
                    onChanged: (value) {
                      _setValue(value ?? defaultValue);
                    },
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: layout['titlePadding'],
                                child: Text(
                                  title,
                                  style: const TextStyle(
                                      fontSize: BS.fontSizeHeading, height: 1),
                                ),
                              ),
                            ),
                          ],
                        ),
                        if (description.isNotEmpty)
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: layout['descriptionPadding'],
                                  child: Text(
                                    description,
                                    style: const TextStyle(
                                        fontSize: BS.fontSizeSmall, height: 1),
                                  ),
                                ),
                              ),
                            ],
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
  }

  /// Update the state of this preference in the app.
  void _setValue(value) {
    sharedPreferences?.setBool(id, value);
    AppState.update('preferences', sharedPreferences, forceRebuild: true);
  }
}
