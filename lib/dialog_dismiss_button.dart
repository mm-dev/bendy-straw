// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';

/// Reusable 'dismiss' button for dialogs.
class DialogDismissButton extends StatelessWidget {
  const DialogDismissButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pop();
      },
      icon: const Icon(Icons.cancel_rounded),
      padding: EdgeInsets.zero,
      alignment: Alignment.topRight,
    );
  }
}
