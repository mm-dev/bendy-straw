// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_state.dart';
import 'constants.dart';
import 'preference_item_boolean.dart';
import 'preference_item_radio_group.dart';
import 'screen.dart';

/// Page/panel for displaying and changing app preferences.
class Preferences extends StatefulWidget {
  const Preferences({super.key});

  @override
  State<Preferences> createState() => _PreferencesState();
}

class _PreferencesState extends AppState<Preferences> {
  /// We want to rebuild when 'preferences' changes, so the widgets eg
  /// checkboxes show their correct current state.
  @override
  List<String>? listenForChanges = ['preferences'];

  @override
  Widget build(BuildContext context) {
    bool screenIsCompact = Screen.isCompact(context);
    var layout = screenIsCompact ? BS.preferenceItemLayoutCompact : BS.preferenceItemLayoutStandard;

    SharedPreferences? prefs = AppState.get('preferences');
    return Material(
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Tooltip(
                message: 'Back',
                child: FloatingActionButton(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(BS.cornerButtonRadius),
                    )),
                    backgroundColor: Theme.of(context).colorScheme.surface,
                    foregroundColor: Theme.of(context).colorScheme.onSurface,
                    heroTag: null,
                    elevation: BS.highButtonElevation,
                    onPressed: () {
                      Navigator.pop(
                        context,
                      );
                    },
                    child: const Icon(Icons.arrow_back_rounded)),
              ),
            ]),
            const Text('Preferences', style: TextStyle(fontSize: BS.fontSizePageHeading)),
            SizedBox(height: layout['between']),

            ///
            PreferenceSectionHeading('Theme'),
            PreferenceItemRadioGroup(
                id: 'brightnessOverride',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Theme brightness',
                options: const {
                  'Dark': 'dark',
                  'Light': 'light',
                  'Follow device settings': 'system',
                }),
            SizedBox(height: layout['between']),
            PreferenceItemBoolean(
                id: 'darkIsTrueBlack',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'True black in dark mode',
                description: 'Eg for OLED displays'),

            ///
            PreferenceSectionHeading('Extra buttons'),
            PreferenceItemBoolean(
                id: 'showJsonImportButton',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Show JSON import button',
                description: 'Enables playlists to be imported via JSON'),
            SizedBox(height: layout['between']),
            PreferenceItemBoolean(
                id: 'showInstantYoutubePlaylistButton',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Show YouTube playlist button',
                description: 'Opens playlist in a browser as a YouTube playlist'),
            SizedBox(height: layout['between']),
            PreferenceItemBoolean(
                id: 'showPlaylistShuffleButton',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Show playlist shuffle button',
                description: 'Shuffles a playlist'),

            ///
            PreferenceSectionHeading('User Interface'),
            PreferenceItemBoolean(
                id: 'shortenUrlsForDisplay',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Shorten URLs for display',
                description: "Eg changes 'https://youtube.com/watch/abcdefg' to '/watch/abcdefg'"),
            PreferenceSectionHeading('Debugging'),
            PreferenceItemBoolean(
                id: 'primaryKeyColumnIsHidden',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Hide primary key in tables',
                description: 'Internal SQLite IDs'),
            SizedBox(height: layout['between']),
            PreferenceItemBoolean(
                id: 'tableUidIsHidden',
                sharedPreferences: prefs,
                screenIsCompact: screenIsCompact,
                title: 'Hide the database UID of local playlists',
                description: 'Internal SQLite ID'),
            SizedBox(
                height:
                    screenIsCompact ? BS.scrollBottomBufferCompact : BS.scrollBottomBufferStandard),
          ]),
        ),
      ),
    );
  }
}

class PreferenceSectionHeading extends StatelessWidget {
  const PreferenceSectionHeading(
    this.text, {
    super.key,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    bool screenIsCompact = Screen.isCompact(context);
    var layout = screenIsCompact ? BS.preferenceItemLayoutCompact : BS.preferenceItemLayoutStandard;

    return Column(
      children: [
        SizedBox(height: layout['between']),
        SizedBox(height: layout['between']),
        Text(text, style: TextStyle(fontSize: BS.fontSizeSectionHeading)),
        SizedBox(height: layout['between']),
      ],
    );
  }
}
