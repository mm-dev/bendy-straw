// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'app_content.dart';
import 'app_state.dart';
import 'constants.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';

void main() {
  runApp(const BendyStrawApp());
}

/// Main app wrapper.
class BendyStrawApp extends StatefulWidget {
  const BendyStrawApp({super.key});

  @override
  State<BendyStrawApp> createState() => _BendyStrawAppState();
}

class _BendyStrawAppState extends AppState<BendyStrawApp> {
  /// We want to rebuild when 'preferences' changes, so the widgets eg
  /// checkboxes show their correct current state.
  @override
  List<String>? listenForChanges = ['preferences'];

  @override
  Widget build(BuildContext context) {
    String brightnessPref =
        AppState.get('preferences')?.getString('brightnessOverride') ??
            BS.defaultPreferences['brightnessOverride'];
    bool darkIsTrueBlack =
        AppState.get('preferences')?.getBool('darkIsTrueBlack') ??
            BS.defaultPreferences['darkIsTrueBlack'];
    return MaterialApp(
        title: BS.appName,
        theme: FlexThemeData.light(scheme: FlexScheme.pinkM3),
        darkTheme: FlexThemeData.dark(
          scheme: FlexScheme.pinkM3,
          darkIsTrueBlack: darkIsTrueBlack,
        ),
        themeMode: brightnessPref == 'dark'
            ? ThemeMode.dark
            : brightnessPref == 'light'
                ? ThemeMode.light
                : ThemeMode.system,
        home: const AppContent());
  }
}
