// SPDX-License-Identifier: GPL-3.0-only

import 'dart:io';

import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';

import 'enum.dart';

/// App constants (`BS` = `BendyStraw`).
///
/// Lots of constants related to the app:
/// - UI (eg colours, sizes)
/// - Defaults for user preferences
/// - Important file paths
/// - Some NewPipe-specific info relating to the database, such as table names
class BS {
  static const String appName = 'BendyStraw';

  /// This directory will be created inside the OS temporary directory and used
  /// for extracting databases and working on them
  static const String baseWorkingDirectory = 'bendy-straw-unzipped';

  /// This gets appended to the original filename (before the file eztension)
  /// on export.
  static const String savedFileSuffix = '-bendy-straw';

  /// Arbitrary choice
  static const playlistNameMaxLength = 255;

  static const Map defaultPreferences = {
    'brightnessOverride': 'system',
    'darkIsTrueBlack': false,
    'showInstantYoutubePlaylistButton': false,
    'showJsonImportButton': false,
    'showPlaylistShuffleButton': false,
    'shortenUrlsForDisplay': true,
    'primaryKeyColumnIsHidden': true,
    'tableUidIsHidden': true,
  };

  ///
  ///
  ///////////////////////
  ///
  /// UI / Layout
  /// Responsive
  ///
  ///////////////////////

  /*
    From:
    https://developer.android.com/guide/topics/large-screens/support-different-screen-sizes

    Compact width 	width < 600dp 	99.96% of phones in portrait
    Medium width 	600dp ≤ width < 840dp 	93.73% of tablets in portrait,

    Large unfolded inner displays in portrait
    Expanded width 	width ≥ 840dp 	97.22% of tablets in landscape,
  */
  static const double breakpointCompact = 600;

  static const Map dialogLayoutCompact = {
    'screenWidthPercent': .9,
    'screenHeightPercent': .9,
    'topPad': BS.paddingMedium,
    'rightPad': BS.paddingMedium,
    'bottomPad': BS.paddingMedium,
    'leftPad': BS.paddingMedium,
    'innerTopPad': BS.paddingSmall,
    'innerRightPad': BS.paddingSmall,
    'innerBottomPad': BS.paddingSmall,
    'innerLeftPad': BS.paddingSmall,
  };
  static const Map dialogLayoutStandard = {
    'screenWidthPercent': .66,
    'screenHeightPercent': .66,
    'topPad': BS.paddingLarge,
    'rightPad': BS.paddingLarge,
    'bottomPad': BS.paddingLarge,
    'leftPad': BS.paddingLarge,
    'innerTopPad': BS.paddingMedium,
    'innerRightPad': BS.paddingMedium,
    'innerBottomPad': BS.paddingMedium,
    'innerLeftPad': BS.paddingMedium,
  };

  static const Map preferenceItemLayoutCompact = {
    'between': BS.paddingSmall,
    'outerPadding': EdgeInsets.all(BS.paddingSmall),
    'titlePadding': EdgeInsets.only(left: BS.paddingExtraSmall),
    'descriptionPadding': EdgeInsets.only(top: BS.paddingExtraSmall, left: BS.paddingSmall),
  };
  static const Map preferenceItemLayoutStandard = {
    'between': BS.paddingMedium,
    'outerPadding': EdgeInsets.all(BS.paddingMedium),
    'titlePadding': EdgeInsets.only(left: BS.paddingSmall),
    'descriptionPadding': EdgeInsets.only(top: BS.paddingSmall, left: BS.paddingSmall),
  };

  static const Map tableLayoutCompact = {
    'checkboxHorizontalMargin': 0.0,
    'horizontalMargin': 0.0,
    'columnSpacing': BS.paddingExtraSmall,
    'headingRowHeight': 24.0,
    'headingTextStyle': TextStyle(height: 1, fontSize: fontSizeExtraSmall),
  };
  static const Map tableLayoutStandard = {
    'checkboxHorizontalMargin': BS.paddingMedium,
    'horizontalMargin': BS.paddingMedium,
    'columnSpacing': BS.paddingSmall,
    'headingRowHeight': 36.0,
    'headingTextStyle': TextStyle(height: 1, fontSize: fontSizeSmall),
  };

  static const Map tableCellLayoutCompact = {
    'padding': EdgeInsets.only(
        left: BS.paddingSmall, top: BS.paddingSuperSmall, bottom: BS.paddingSuperSmall),
    'textStyle': TextStyle(height: 1, fontSize: fontSizeExtraSmall),
    'urlButtonPadding':
        EdgeInsets.symmetric(horizontal: BS.paddingSuperSmall, vertical: BS.paddingSuperSmall),
  };
  static const Map tableCellLayoutStandard = {
    'padding': EdgeInsets.only(
        left: BS.paddingExtraSmall, top: BS.paddingExtraSmall, bottom: BS.paddingExtraSmall),
    'textStyle': TextStyle(height: 1, fontSize: fontSizeSmall),
    'urlButtonPadding':
        EdgeInsets.symmetric(horizontal: BS.paddingExtraSmall, vertical: BS.paddingExtraSmall),
  };

  static ButtonStyle buttonStyleCompact = ElevatedButton.styleFrom(
    minimumSize: Size.zero,
    visualDensity: VisualDensity.compact,
    padding: const EdgeInsets.all(BS.paddingIconCompact),
    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
  );
  static ButtonStyle buttonStyleStandard = ElevatedButton.styleFrom(
    visualDensity: VisualDensity.compact,
    padding: const EdgeInsets.all(BS.paddingIconStandard),
  );

  static const double scrollBottomBufferStandard = 150;
  static const double scrollBottomBufferCompact = 75;

  static const double paddingIconStandard = 1;
  static const double paddingIconCompact = 14;

  ///
  ///
  ///////////////////////
  ///
  /// UI / Layout
  ///
  ///////////////////////

  static const double paddingSuperSmall = 2;
  static const double paddingExtraSmall = 4;
  static const double paddingSmall = 6;
  static const double paddingMedium = 16;
  static const double paddingMediumLarge = 24;
  static const double paddingLarge = 32;
  static const double paddingExtraLarge = 64;

  /// Used to prevent tabs (especially the [X] button) disappearing underneath
  /// the settings button.
  static const double tabBarRightPadding = 56;

  static const double cornerRadius = 8;
  static const double cornerButtonRadius = 36;

  static const double renamableTextFieldMaxWidth = 300;
  static const double renamableTextFieldMinWidth = 150;

  static const double checkboxBorderWidth = 2;
  static const double tableWidgetHeight = 500;
  static const double tabHeight = 22;
  static const double playlistItemCountWidth = 36;
  static const double buttonMaxWidth = 200;
  static const double moveModeTextMinWidth = 50;
  static const double moveModeTextMaxHeight = 36;

  /// Used eg for add/export buttons which hover above the rest of the UI
  static const double highButtonElevation = 13;

  static const double disableControlOpacity = 0.4;

  /// List of destinations eg playlists that can be copied to.
  static const double destinationSingleItemHeight = 108;
  static const double destinationListHeight = 250;
  static const double destinationListWidth = 250;

  static const double fontSizePageHeading = 28;
  static const double fontSizeSectionHeading = 24;
  static const double fontSizeNumberIcon = 24;
  static const double fontSizeBaseInfo = 18;
  static const double fontSizeHeading = 20;
  static const double fontSizeSmall = 13;
  static const double fontSizeExtraSmall = 11;
  static const double fontSizePlaylistItemCount = 14;
  static const double fontSizePlaylistName = 15;

  static const double numberIconSize = 30;
  static const double infoRowLineHeight = 1.3;
  static const double infoRowGapLineHeight = 0.6;

  /// Database tabs are coloured according to this list, in sequence, looping
  /// back to the start if more than [flexSchemeColors.length] tabs are opened.
  static const List<FlexScheme> flexSchemeColors = [
    FlexScheme.redM3,
    FlexScheme.greenM3,
    FlexScheme.blueM3,
    FlexScheme.yellowM3,
    FlexScheme.purpleM3,
    FlexScheme.tealM3,
  ];

  ///
  ///
  ///////////////////////
  ///
  /// Paths to show in the file picker
  ///
  ///////////////////////

  static final List<String> filepathsLinux = [
    Platform.environment['HOME'] ?? '/',
  ];

  static final List<String> filepathsWindows = [
    Platform.environment['USERPROFILE'] ?? '/',
  ];

  static const List<String> filepathsAndroid = [
    '/sdcard/',
    '/sdcard/Download',
    '/storage/emulated/0/',
    '/storage/emulated/0/Download',
  ];

  ///
  ///
  ///////////////////////
  ///
  /// SQL/database related data below
  ///
  ///////////////////////

  /// Table names as used in NewPipe databases
  static const String streamsTableId = 'streams';

  static const String localPlaylistsTableId = 'playlists';
  static const String remotePlaylistsTableId = 'remote_playlists';
  static const String subscriptionsTableId = 'subscriptions';
  static const String joinTableId = 'playlist_stream_join';

  /// Internal name for the special temporary table used to join streams with
  /// their playlists
  static const String tempPlaylistStreamJoinedTableId = 'tempPlaylistStreamJoinedTable';

  /// Used to check if the database is a compatible version (it must contain
  /// all of these tables).
  static const List<String> requiredTables = [
    'streams',
    'playlists',
    //'nonexistent_table_for_testing',
    'playlist_stream_join',
    'remote_playlists',
    'subscriptions',
  ];

  static const Map<String, Map<String, dynamic>> knownTables = {
    'remote_playlists': {
      'displayName': 'Bookmarked Playlists',
      'tableType': TableType.remotePlaylists,
      'columnList': RemotePlaylistsColumns.values,
    },
    'subscriptions': {
      'displayName': 'Channels',
      'tableType': TableType.subscriptions,
      'columnList': SubscriptionsColumns.values,
    },
  };
}
