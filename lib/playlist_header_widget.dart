// SPDX-License-Identifier: GPL-3.0-only

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'constants.dart';
import 'db_manager.dart';
import 'playlist_buttons_widget.dart';

/// Header for a playlist.
///
/// Containing:
/// - The playlist name, in an editable [TextField]
/// - Playlist-specific buttons
class PlaylistHeaderWidget extends StatefulWidget {
  final String name;
  final String dbPath;
  final String tableId;

  const PlaylistHeaderWidget({
    super.key,
    required this.dbPath,
    required this.tableId,
    required this.name,
  });

  @override
  State<PlaylistHeaderWidget> createState() => _PlaylistHeaderWidgetState();
}

class _PlaylistHeaderWidgetState extends State<PlaylistHeaderWidget> {
  late final TextEditingController _textEditingController =
      TextEditingController(text: widget.name);

  late Color _renamableTextColor;
  late Color _renamableTextBackgroundColor;
  late bool _areUnsavedChanges = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _renamableTextColor = Theme.of(context).colorScheme.primary;
    _renamableTextBackgroundColor = Theme.of(context).colorScheme.onPrimary;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              runAlignment: WrapAlignment.spaceBetween,
              alignment: WrapAlignment.spaceBetween,
              children: [
                ///
                ///
                /// Editable playlist name.
                ///
                ConstrainedBox(
                  constraints: const BoxConstraints(
                      minWidth: BS.renamableTextFieldMinWidth,
                      maxWidth: BS.renamableTextFieldMaxWidth),
                  child: TextField(
                    style: TextStyle(
                        fontSize: BS.fontSizePlaylistName,

                        /// We want to match the height of the elevated buttons,
                        /// which by default are 36px high. [TextStyle.height] is a
                        /// multiple of the field's [fontSize]
                        height: 36 /
                            (BS.fontSizePlaylistName + BS.paddingExtraSmall),
                        leadingDistribution: TextLeadingDistribution.even,
                        color: _renamableTextColor,
                        fontWeight: FontWeight.bold),
                    controller: _textEditingController,
                    inputFormatters: [
                      /// Use this instead of [TextField.maxLength] as we don't
                      /// want to display a character count.
                      LengthLimitingTextInputFormatter(BS.playlistNameMaxLength)
                    ],
                    decoration: InputDecoration(
                      fillColor: _renamableTextBackgroundColor,
                      isCollapsed: true,
                      contentPadding: const EdgeInsets.only(
                        top: BS.paddingSmall,
                        right: BS.paddingSmall,
                        bottom: BS.paddingSmall,
                        left: BS.paddingMedium,
                      ),
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide:
                            const BorderSide(width: 0, style: BorderStyle.none),
                      ),
                    ),
                    onChanged: (String value) {
                      _renamableTextBackgroundColor =
                          Theme.of(context).colorScheme.primary;
                      _renamableTextColor =
                          Theme.of(context).colorScheme.onPrimary;
                      _areUnsavedChanges = true;
                      setState(() {});
                    },
                    onSubmitted: _renameLocalPlaylist,
                    onTapOutside: (PointerDownEvent event) {
                      if (_areUnsavedChanges) {
                        _renameLocalPlaylist(_textEditingController.text);
                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                      }
                    },
                  ),
                ),

                ///
                ///
                /// Buttons.
                ///
                PlaylistButtonsWidget(
                    playlistDisplayName: widget.name,
                    playlistTableId: widget.tableId,
                    playlistDbPath: widget.dbPath)
              ]),
        ),
      ],
    );
  }

  /// The name of the playlist has been edited in the [TextField], so update
  /// the database to reflect the change.
  void _renameLocalPlaylist(String newTableName) {
    DbManager.renameLocalPlaylist(widget.dbPath, widget.tableId, newTableName);
    _textEditingController.text = newTableName;
    _renamableTextColor = Theme.of(context).colorScheme.primary;
    _renamableTextBackgroundColor = Theme.of(context).colorScheme.onPrimary;
    _areUnsavedChanges = false;
    setState(() {});
  }
}
