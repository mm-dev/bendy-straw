### No pull requests please

**It's better to fork than to submit a pull request**. I'm unlikely to accept a PR and don't want you to waste your time.


I don't mean to be rude or unhelpful, but:

- I'm not great at managing other people
- I want to enjoy ownership of my personal projects


I love writing code; managing contributions feels like a different job. I hear about many OSS maintainers becoming joyless and I don't want to end up like that. 


I want interested people to be able to see and change the code that their software is built from. I don't want to manage that process though, it's not for me.



### Fork it instead

I support forking and can offer advice and answer questions.



### Bug reports and feature requests

I'm grateful for bug reports and feature suggestions.
